package com.eale.inteligentcloud.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * 生产跟踪(报工单)
 * @author Administrator
 *
 */
@Entity
public class ICPFollow implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4736794031027489275L;

	/**
	 * 报工单ID,PT+当前时间戳  主键
	 */
	@Id
	@GeneratedValue
	private Long followId;
	
	/**
	 * 来源生产调度  工单 一对一
	 */
	@OneToOne
	private ICPDispatch followDispatch;
	
	/**
	 * 来源订单
	 */
	@OneToOne
	private ICPOrder followOrder;
	
	/**
	 * 报告离上一次报告的加工数量
	 */
	private Integer followProcessNum;
	
	/**
	 * 报告离上一次报告的合格数量
	 */
	private Integer followQualifyNum;
	
	/**
	 * 这一次加工的开始时间
	 */
	private Long followStratTime;
	
	/**
	 * 这一次加工的结束时间
	 */
	private Long followEndTime;
	
	/**
	 * 是[1]/否[-1]  如果结束报工处选择是，则结束当前工单报工状态，置当前工单状态为完成
	 */
	private Integer followType;
	
	/**
	 * 是否已经报工   是[1] 否[-1]
	 */
	private Integer followState;
	
	/**
	 * 创建时间
	 */
	private Long followCtime;
	
	/**
	 * 创建用户
	 */
	private Long followCuser;
	
	/**
	 * 修改时间
	 */
	private Long followUptime;
	
	/**
	 * 修改用户
	 */
	private Long followUpuser;

	public Long getFollowId() {
		return followId;
	}

	public void setFollowId(Long followId) {
		this.followId = followId;
	}

	public ICPDispatch getFollowDispatch() {
		return followDispatch;
	}

	public void setFollowDispatch(ICPDispatch followDispatch) {
		this.followDispatch = followDispatch;
	}

	public Integer getFollowProcessNum() {
		return followProcessNum;
	}

	public void setFollowProcessNum(Integer followProcessNum) {
		this.followProcessNum = followProcessNum;
	}

	public Integer getFollowQualifyNum() {
		return followQualifyNum;
	}

	public void setFollowQualifyNum(Integer followQualifyNum) {
		this.followQualifyNum = followQualifyNum;
	}

	public Long getFollowStratTime() {
		return followStratTime;
	}

	public void setFollowStratTime(Long followStratTime) {
		this.followStratTime = followStratTime;
	}

	public Long getFollowEndTime() {
		return followEndTime;
	}

	public void setFollowEndTime(Long followEndTime) {
		this.followEndTime = followEndTime;
	}

	public Integer getFollowType() {
		return followType;
	}

	public void setFollowType(Integer followType) {
		this.followType = followType;
	}

	public Long getFollowCtime() {
		return followCtime;
	}

	public void setFollowCtime(Long followCtime) {
		this.followCtime = followCtime;
	}

	public Long getFollowCuser() {
		return followCuser;
	}

	public void setFollowCuser(Long followCuser) {
		this.followCuser = followCuser;
	}

	public Long getFollowUptime() {
		return followUptime;
	}

	public void setFollowUptime(Long followUptime) {
		this.followUptime = followUptime;
	}

	public Long getFollowUpuser() {
		return followUpuser;
	}

	public void setFollowUpuser(Long followUpuser) {
		this.followUpuser = followUpuser;
	}


	public Integer getFollowState() {
		return followState;
	}

	public void setFollowState(Integer followState) {
		this.followState = followState;
	}
	
	
	public ICPOrder getFollowOrder() {
		return followOrder;
	}

	public void setFollowOrder(ICPOrder followOrder) {
		this.followOrder = followOrder;
	}

	@Override
	public String toString() {
		return "ICPFollow [followId=" + followId + ", followDispatch=" + followDispatch + ", followOrder=" + followOrder
				+ ", followProcessNum=" + followProcessNum + ", followQualifyNum=" + followQualifyNum
				+ ", followStratTime=" + followStratTime + ", followEndTime=" + followEndTime + ", followType="
				+ followType + ", followState=" + followState + ", followCtime=" + followCtime + ", followCuser="
				+ followCuser + ", followUptime=" + followUptime + ", followUpuser=" + followUpuser + "]";
	}

	

	
}
