package com.eale.inteligentcloud.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * 生产调度(工单)
 * @author Administrator
 *
 */
@Entity
public class ICPDispatch {
	
	/**
	 * 生产调度id,主键
	 */
	@Id
	@GeneratedValue
	private Long dispFid;
	
	/**
	 * 来源生产计划
	 */
	@OneToOne
	private ICPProductionPlan disProductionPlan;
	
	/**
	 * 来源设备管理
	 */
	@OneToOne
	private ICPEquipment disequipment;

	/**
	 * 开始日期
	 */
	private Long dispBegintiem;
	
	/**
	 * 结束时间
	 */
	private Long dispEndtime;
	
	/**
	 * 状态:未启动[1]，生产中[2]，已完成[3] 失效[-1]
	 */
	private Integer dispState;
	
	
	
	/**
	 * 创建时间
	 */
	private Long dispCtime;
	
	/**
	 * 创建用户
	 */
	private Long dispCuser;
	
	/**
	 * 修改时间
	 */
	private Long dispUptime;
	
	/**
	 * 修改用户
	 */
	private Long dispUpuser;
	
	/**
	 * 来源报工单   一对多
	 */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="dispFid")
	private List<ICPFollow> follows;

	public Long getDispFid() {
		return dispFid;
	}

	public void setDispFid(Long dispFid) {
		this.dispFid = dispFid;
	}

	public Long getDispBegintiem() {
		return dispBegintiem;
	}

	public void setDispBegintiem(Long dispBegintiem) {
		this.dispBegintiem = dispBegintiem;
	}

	public Long getDispEndtime() {
		return dispEndtime;
	}

	public void setDispEndtime(Long dispEndtime) {
		this.dispEndtime = dispEndtime;
	}

	public Integer getDispState() {
		return dispState;
	}

	public void setDispState(Integer dispState) {
		this.dispState = dispState;
	}

	public Long getDispCtime() {
		return dispCtime;
	}

	public void setDispCtime(Long dispCtime) {
		this.dispCtime = dispCtime;
	}

	public Long getDispCuser() {
		return dispCuser;
	}

	public void setDispCuser(Long dispCuser) {
		this.dispCuser = dispCuser;
	}

	public Long getDispUptime() {
		return dispUptime;
	}

	public void setDispUptime(Long dispUptime) {
		this.dispUptime = dispUptime;
	}

	public Long getDispUpuser() {
		return dispUpuser;
	}

	public void setDispUpuser(Long dispUpuser) {
		this.dispUpuser = dispUpuser;
	}

	public List<ICPFollow> getFollows() {
		return follows;
	}

	public void setFollows(List<ICPFollow> follows) {
		this.follows = follows;
	}
	
	public ICPProductionPlan getDisProductionPlan() {
		return disProductionPlan;
	}

	public void setDisProductionPlan(ICPProductionPlan disProductionPlan) {
		this.disProductionPlan = disProductionPlan;
	}

	public ICPEquipment getDisequipment() {
		return disequipment;
	}

	public void setDisequipment(ICPEquipment disequipment) {
		this.disequipment = disequipment;
	}

	@Override
	public String toString() {
		return "ICPDispatch [dispFid=" + dispFid + ", disProductionPlan=" + disProductionPlan + ", disequipment="
				+ disequipment + ", dispBegintiem=" + dispBegintiem + ", dispEndtime=" + dispEndtime + ", dispState="
				+ dispState + ", dispCtime=" + dispCtime + ", dispCuser=" + dispCuser + ", dispUptime=" + dispUptime
				+ ", dispUpuser=" + dispUpuser + ", follows=" + follows + "]";
	}

	
}
