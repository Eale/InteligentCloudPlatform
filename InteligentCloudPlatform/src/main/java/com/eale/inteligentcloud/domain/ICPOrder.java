package com.eale.inteligentcloud.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * 订单
 * @author Administrator
 *
 */
@Entity
public class ICPOrder {
	
	/**
	 * 主键
	 */
	@Id
	@GeneratedValue
	private Long oId;
	
	/**
	 * O开头+时间戳来标识
	 */
	private Long oNumber;
	
	/**
	 * 来源:线上[1]/线下[2]
	 */
	private Integer oSourse;
	
	/**
	 * 订单产品,来源产品
	 */
//	@OneToMany(fetch=FetchType.LAZY)
//	@JoinColumn(name="OrderId")
	@OneToOne
	private ICPProduct oProduct ;
	
	/**
	 * 下单的数量
	 */
	private Long oQuantity;
	
	/**
	 * 截止时间完成(交货)
	 */
	private Long oDeadline;
	
	/**
	 * 状态:待接单[1]/已拒绝[2]/已接单[3]/生产中[4]/已完成[5]/删除[-1]
	 */
	private Integer oState;
	
	/**
	 * 完成数量  来源于报工单 嵌入@emdeddable
	 */
	private Integer oFinishNum;
	
	/**
	 * 拒绝订单的理由
	 */
	private String oRemark;
	
	/**
	 * 下单的时间
	 */
	private Long oPlacetime;
	
	/**
	 * 创建时间
	 */
	private Long oCtime;
	
	/**
	 * 创建用户
	 */
	private Long oCuser;
	
	/**
	 * 修改时间
	 */
	private Long oUptime;
	
	/**
	 * 修改用户
	 */
	private Long oUpuser;

	public Long getoId() {
		return oId;
	}

	public void setoId(Long oId) {
		this.oId = oId;
	}

	public Integer getoSourse() {
		return oSourse;
	}

	public void setoSourse(Integer oSourse) {
		this.oSourse = oSourse;
	}

	public ICPProduct getoProduct() {
		return oProduct;
	}

	public void setoProduct(ICPProduct oProduct) {
		this.oProduct = oProduct;
	}

	public Long getoQuantity() {
		return oQuantity;
	}

	public void setoQuantity(Long oQuantity) {
		this.oQuantity = oQuantity;
	}

	public Long getoDeadline() {
		return oDeadline;
	}

	public void setoDeadline(Long oDeadline) {
		this.oDeadline = oDeadline;
	}

	public Integer getoState() {
		return oState;
	}

	public void setoState(Integer oState) {
		this.oState = oState;
	}

	public Integer getoFinishNum() {
		return oFinishNum;
	}

	public void setoFinishNum(Integer oFinishNum) {
		this.oFinishNum = oFinishNum;
	}

	public String getoRemark() {
		return oRemark;
	}

	public void setoRemark(String oRemark) {
		this.oRemark = oRemark;
	}

	public Long getoPlacetime() {
		return oPlacetime;
	}

	public void setoPlacetime(Long oPlacetime) {
		this.oPlacetime = oPlacetime;
	}

	public Long getoCtime() {
		return oCtime;
	}

	public void setoCtime(Long oCtime) {
		this.oCtime = oCtime;
	}

	public Long getoCuser() {
		return oCuser;
	}

	public void setoCuser(Long oCuser) {
		this.oCuser = oCuser;
	}

	public Long getoUptime() {
		return oUptime;
	}

	public void setoUptime(Long oUptime) {
		this.oUptime = oUptime;
	}

	public Long getoUpuser() {
		return oUpuser;
	}

	public void setoUpuser(Long oUpuser) {
		this.oUpuser = oUpuser;
	}


	public Long getoNumber() {
		return oNumber;
	}

	public void setoNumber(Long oNumber) {
		this.oNumber = oNumber;
	}

	@Override
	public String toString() {
		return "Order [oId=" + oId + ", oNumber=" + oNumber + ", oSourse=" + oSourse + ", oProduct=" + oProduct
				+ ", oQuantity=" + oQuantity + ", oDeadline=" + oDeadline + ", oState=" + oState + ", oFinishNum="
				+ oFinishNum + ", oRemark=" + oRemark + ", oPlacetime=" + oPlacetime + ", oCtime=" + oCtime
				+ ", oCuser=" + oCuser + ", oUptime=" + oUptime + ", oUpuser=" + oUpuser + "]";
	}

	
	
	
	
}
