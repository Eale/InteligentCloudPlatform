package com.eale.inteligentcloud.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * 管理员个人信息
 * @author Administrator
 *
 */
@Entity
public class ICPPersonalInformation {
	/**
	 * 管理员个人ID , 主键  后台自动删除
	 */
	@Id
	@GeneratedValue
	private Long perId;
	
	/**
	 * 名字
	 */
	private String perName;
	
	/**
	 * 性别
	 */
	private Integer perGender;
	
	/**
	 * 手机号
	 */
	private Long perPhone;
	
	/**
	 * 邮箱
	 */
	private String perEmail;
	
	/**
	 * 简历,附件
	 */
	private String perAnlage;
	
	/**
	 * 所在城市
	 */
	private String perCity;
	
	/**
	 * 备注
	 */
	private String perRemark;
    /**
     * 创建时间
     */
    private Long perCtime;
    /**
     * 创建用户
     */
    private Long perCuser;
    /**
     * 修改时间
     */
    private Long  perUptime;
    /**
     * 修改用户
     */
    private Long perUpuser;
    
    /**
     * 所属管理员
     */
    @OneToOne
    private ICPAdminstrator admins;

    private Integer state;
    /**
     * 无参构造
     */
    public ICPPersonalInformation() {
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public String getPerName() {
        return perName;
    }

    public void setPerName(String perName) {
        this.perName = perName;
    }

    public Integer getPerGender() {
        return perGender;
    }

    public void setPerGender(Integer perGender) {
        this.perGender = perGender;
    }

    public Long getPerPhone() {
        return perPhone;
    }

    public void setPerPhone(Long perPhone) {
        this.perPhone = perPhone;
    }

    public String getPerEmail() {
        return perEmail;
    }

    public void setPerEmail(String perEmail) {
        this.perEmail = perEmail;
    }

    public String getPerAnlage() {
        return perAnlage;
    }

    public void setPerAnlage(String perAnlage) {
        this.perAnlage = perAnlage;
    }

    public String getPerCity() {
        return perCity;
    }

    public void setPerCity(String perCity) {
        this.perCity = perCity;
    }

    public String getPerRemark() {
        return perRemark;
    }

    public void setPerRemark(String perRemark) {
        this.perRemark = perRemark;
    }

    public Long getPerCtime() {
        return perCtime;
    }

    public void setPerCtime(Long perCtime) {
        this.perCtime = perCtime;
    }

    public Long getPerCuser() {
        return perCuser;
    }

    public void setPerCuser(Long perCuser) {
        this.perCuser = perCuser;
    }

    public Long getPerUptime() {
        return perUptime;
    }

    public void setPerUptime(Long perUptime) {
        this.perUptime = perUptime;
    }

    public Long getPerUpuser() {
        return perUpuser;
    }

    public void setPerUpuser(Long perUpuser) {
        this.perUpuser = perUpuser;
    }

	public ICPAdminstrator getAdmins() {
		return admins;
	}

	public void setAdmins(ICPAdminstrator admins) {
		this.admins = admins;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "ICPPersonalInformation [perId=" + perId + ", perName=" + perName + ", perGender=" + perGender
				+ ", perPhone=" + perPhone + ", perEmail=" + perEmail + ", perAnlage=" + perAnlage + ", perCity="
				+ perCity + ", perRemark=" + perRemark + ", perCtime=" + perCtime + ", perCuser=" + perCuser
				+ ", perUptime=" + perUptime + ", perUpuser=" + perUpuser + ", admins=" + admins + ", state=" + state
				+ "]";
	}

	
	
	
}
