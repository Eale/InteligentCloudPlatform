package com.eale.inteligentcloud.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * 生产计划
 * @author Administrator
 *
 */
@Entity
public class ICPProductionPlan {

    /**
	 * 	后台生成|生产计划id , 主键
	 */
	@Id
	@GeneratedValue
	private Long planId;
	
	/**
	 * 来源订单,@OneToOne
	 */
	@OneToOne
	private ICPOrder planOrder;
	
	/**
	 * 计划数量
	 */
	private Long planNumber;
	
	/**
	 * 计划开始时间
	 */
	private Long planStarttime;
	
	/**
	 * 计划结束时间
	 */
	private Long planEndtime;
	
	
	/**
	 * 状态:未启动[1]/已启动[2]/完成[3]/删除[-1]
	 */
	private Integer planState;
	
	/**
	 * 生成生产计划的时间
	 */
	private Long planTime;

    /**
     * 创建时间
     */
    private Long planCtime;
    /**
     * 创建用户
     */
    private Long planCuser;
    /**
     * 修改时间
     */
    private Long  planUptime;
    /**
     * 修改用户
     */
    private Long planUpuser;

    /**
     * 无参构造
     */
    public ICPProductionPlan() {
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public ICPOrder getPlanOrder() {
        return planOrder;
    }

    public void setPlanOrder(ICPOrder planOrder) {
        this.planOrder = planOrder;
    }

    public Long getPlanStarttime() {
        return planStarttime;
    }

    public void setPlanStarttime(Long planStarttime) {
        this.planStarttime = planStarttime;
    }

    public Long getPlanEndtime() {
        return planEndtime;
    }

    public void setPlanEndtime(Long planEndtime) {
        this.planEndtime = planEndtime;
    }


    public Integer getPlanState() {
        return planState;
    }

    public void setPlanState(Integer planState) {
        this.planState = planState;
    }

    public Long getPlanTime() {
        return planTime;
    }

    public void setPlanTime(Long planTime) {
        this.planTime = planTime;
    }

    public Long getPlanCtime() {
        return planCtime;
    }

    public void setPlanCtime(Long planCtime) {
        this.planCtime = planCtime;
    }

    public Long getPlanCuser() {
        return planCuser;
    }

    public void setPlanCuser(Long planCuser) {
        this.planCuser = planCuser;
    }

    public Long getPlanUptime() {
        return planUptime;
    }

    public void setPlanUptime(Long planUptime) {
        this.planUptime = planUptime;
    }

    public Long getPlanUpuser() {
        return planUpuser;
    }

    public void setPlanUpuser(Long planUpuser) {
        this.planUpuser = planUpuser;
    }

	public Long getPlanNumber() {
		return planNumber;
	}

	public void setPlanNumber(Long planNumber) {
		this.planNumber = planNumber;
	}
}
