package com.eale.inteligentcloud.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;


/**
 * 管理员
 * @author Administrator
 *
 */
@Entity
public class ICPAdminstrator {
	
	//@Id表的主键
	//@GeneratedValue主键生成策略,默认情况下,MySQL使用自增字段,Oracle使用序列
	@Id
	@GeneratedValue
	private Long adId;
	
	/**
	 * 管管理员的编号 , 登录的凭证 
	 */
	@Column(unique=true)
	private String adNumber;
	
	/**
	 * 登录的密码
	 */
	private String adPassword;
	
	/**
	 * 等级:超级管理员[1]/管理员[2]/普通员工[3]   解决权限问题
	 */
	private Integer adGrade;
	
	/**
	 * 删除改变状态:存活[-1]/删除状态[1]
	 */
	private Integer adDelete;
	
	/**
	 * 创建时间
	 */
	private Long adCtime;
	
	/**
	 * 创建用户
	 */
	private Long adCuser;
	
	/**
	 * 修改时间
	 */
	private Long adUptime;
	
	/**
	 * 修改用户
	 */
	private Long adUpuser;
	
	/**
	 * 所属工厂
	 */
	@OneToOne
	private ICPFactory adfactory;
	
//	/**
//	 * 个人信息 
//	 */
//	@OneToOne
//	private ICPPersonalInformation personalInformation;
	
	

	public Long getAdId() {
		return adId;
	}

	public void setAdId(Long adId) {
		this.adId = adId;
	}

	public String getAdNumber() {
		return adNumber;
	}

	public void setAdNumber(String adNumber) {
		this.adNumber = adNumber;
	}

	public String getAdPassword() {
		return adPassword;
	}

	public void setAdPassword(String adPassword) {
		this.adPassword = adPassword;
	}

	public Integer getAdGrade() {
		return adGrade;
	}

	public void setAdGrade(Integer adGrade) {
		this.adGrade = adGrade;
	}

	public Integer getAdDelete() {
		return adDelete;
	}

	public void setAdDelete(Integer adDelete) {
		this.adDelete = adDelete;
	}

	public Long getAdCtime() {
		return adCtime;
	}

	public void setAdCtime(Long adCtime) {
		this.adCtime = adCtime;
	}

	public Long getAdCuser() {
		return adCuser;
	}

	public void setAdCuser(Long adCuser) {
		this.adCuser = adCuser;
	}

	public Long getAdUptime() {
		return adUptime;
	}

	public void setAdUptime(Long adUptime) {
		this.adUptime = adUptime;
	}

	public Long getAdUpuser() {
		return adUpuser;
	}

	public void setAdUpuser(Long adUpuser) {
		this.adUpuser = adUpuser;
	}

//	public ICPPersonalInformation getPersonalInformation() {
//		return personalInformation;
//	}
//
//	public void setPersonalInformation(ICPPersonalInformation personalInformation) {
//		this.personalInformation = personalInformation;
//	}


	public ICPFactory getAdfactory() {
		return adfactory;
	}

	public void setAdfactory(ICPFactory adfactory) {
		this.adfactory = adfactory;
	}

	@Override
	public String toString() {
		return "ICPAdminstrator [adId=" + adId + ", adNumber=" + adNumber + ", adPassword=" + adPassword + ", adGrade="
				+ adGrade + ", adDelete=" + adDelete + ", adCtime=" + adCtime + ", adCuser=" + adCuser + ", adUptime="
				+ adUptime + ", adUpuser=" + adUpuser + ", adfactory=" + adfactory + "]";
	}

//	@Override
//	public String toString() {
//		return "ICPAdminstrator [adId=" + adId + ", adNumber=" + adNumber + ", adPassword=" + adPassword + ", adGrade="
//				+ adGrade + ", adDelete=" + adDelete + ", adCtime=" + adCtime + ", adCuser=" + adCuser + ", adUptime="
//				+ adUptime + ", adUpuser=" + adUpuser + ", adfactory=" + adfactory + ", personalInformation="
//				+ personalInformation + "]";
//	}
	
	
	
	
}
