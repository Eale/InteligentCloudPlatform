package com.eale.inteligentcloud.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import javax.persistence.Entity;

/**
 * 设备
 * @author Administrator
 *
 */
@Entity
public class ICPEquipment {
	@Id
	@GeneratedValue
	private Long equipId;
	
	/**
	 * 设备序列号
	 */
	
	private String equipSequence;

	
	/**
	 * 设备名称
	 */
	private String equipName;
	
	/**
	 * 设备图片
	 */
	private String equipImgur;
	
	/**
	 * 设备状态 :未启用[0]/待机[1]/运行[2]/停机[3]/故障[4]  失效[-1]
	 */
	private Integer equipState;

	/**
	 * 可生产产品 一对一
	 */
	@OneToOne
	private ICPProduct equipProduct;
	
	/**
	 * 设备安装时间
	 */
	private Long equipTime;

	/**
	 * 产能
	 */
	private Double equipCapacity;

	/**
	 * 创建时间
	 */
	private Long equipCtime;

	/**
	 * 创建用户
	 */
	private Long equipCuser;
	
	/**
	 * 修改时间
	 */
	private Long equipUptime;

	/**
	 * 修改用户
	 */
	private Long equipUpuser;
	
	/**
	 * 所属工厂
	 */
	@OneToOne
	private ICPFactory equipfactory;

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public String getEquipSequence() {
		return equipSequence;
	}

	public void setEquipSequence(String equipSequence) {
		this.equipSequence = equipSequence;
	}

	public String getEquipName() {
		return equipName;
	}

	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}

	public String getEquipImgur() {
		return equipImgur;
	}

	public void setEquipImgur(String equipImgur) {
		this.equipImgur = equipImgur;
	}

	public Integer getEquipState() {
		return equipState;
	}

	public void setEquipState(Integer equipState) {
		this.equipState = equipState;
	}

	public ICPProduct getEquipProduct() {
		return equipProduct;
	}

	public void setEquipProduct(ICPProduct equipProduct) {
		this.equipProduct = equipProduct;
	}

	public Long getEquipTime() {
		return equipTime;
	}

	public void setEquipTime(Long equipTime) {
		this.equipTime = equipTime;
	}

	public Double getEquipCapacity() {
		return equipCapacity;
	}

	public void setEquipCapacity(Double equipCapacity) {
		this.equipCapacity = equipCapacity;
	}

	public Long getEquipCtime() {
		return equipCtime;
	}

	public void setEquipCtime(Long equipCtime) {
		this.equipCtime = equipCtime;
	}

	public Long getEquipCuser() {
		return equipCuser;
	}

	public void setEquipCuser(Long equipCuser) {
		this.equipCuser = equipCuser;
	}

	public Long getEquipUptime() {
		return equipUptime;
	}

	public void setEquipUptime(Long equipUptime) {
		this.equipUptime = equipUptime;
	}

	public Long getEquipUpuser() {
		return equipUpuser;
	}

	public void setEquipUpuser(Long equipUpuser) {
		this.equipUpuser = equipUpuser;
	}


	public ICPFactory getEquipfactory() {
		return equipfactory;
	}

	public void setEquipfactory(ICPFactory equipfactory) {
		this.equipfactory = equipfactory;
	}

	@Override
	public String toString() {
		return "ICPEquipment [equipId=" + equipId + ", equipSequence=" + equipSequence + ", equipName=" + equipName
				+ ", equipImgur=" + equipImgur + ", equipState=" + equipState + ", equipProduct=" + equipProduct
				+ ", equipTime=" + equipTime + ", equipCapacity=" + equipCapacity + ", equipCtime=" + equipCtime
				+ ", equipCuser=" + equipCuser + ", equipUptime=" + equipUptime + ", equipUpuser=" + equipUpuser
				+ ", equipfactory=" + equipfactory + "]";
	}
	
	
	

}
