package com.eale.inteligentcloud.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 工厂
 * @author Administrator
 *
 */
@Entity
public class ICPFactory {
	
	/**
	 * 主键 , 后台生成
	 */
	@Id
	@GeneratedValue
	private Long fId;
	
	/**
	 * 工厂名字
	 */
	private String fName;
	

	/**
	 * 工厂 状态:正常[1/失效[-1]
	 */
	private Integer fStatu;
	
	/**
	 * 创建时间
	 */
	private Long fCtime;
	
	/**
	 * 创建用户
	 */
	private Long fCuser;
	
	/**
	 * 修改时间
	 */
	private Long fUptime;
	
	/**
	 * 修改用户
	 */
	private Long fUpuser;

	public Long getfId() {
		return fId;
	}

	public void setfId(Long fId) {
		this.fId = fId;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

//	public List<ICPEquipment> getfEquipment() {
//		return fEquipment;
//	}
//
//	public void setfEquipment(List<ICPEquipment> fEquipment) {
//		this.fEquipment = fEquipment;
//	}
//
//	public List<ICPAdminstrator> getfAdminstrator() {
//		return fAdminstrator;
//	}
//
//	public void setfAdminstrator(List<ICPAdminstrator> fAdminstrator) {
//		this.fAdminstrator = fAdminstrator;
//	}
//
//	public List<ICPProduct> getfProduct() {
//		return fProduct;
//	}
//
//	public void setfProduct(List<ICPProduct> fProduct) {
//		this.fProduct = fProduct;
//	}

	public Integer getfStatu() {
		return fStatu;
	}

	public void setfStatu(Integer fStatu) {
		this.fStatu = fStatu;
	}

	public Long getfCtime() {
		return fCtime;
	}

	public void setfCtime(Long fCtime) {
		this.fCtime = fCtime;
	}

	public Long getfCuser() {
		return fCuser;
	}

	public void setfCuser(Long fCuser) {
		this.fCuser = fCuser;
	}

	public Long getfUptime() {
		return fUptime;
	}

	public void setfUptime(Long fUptime) {
		this.fUptime = fUptime;
	}

	public Long getfUpuser() {
		return fUpuser;
	}

	public void setfUpuser(Long fUpuser) {
		this.fUpuser = fUpuser;
	}

	@Override
	public String toString() {
		return "ICPFactory [fId=" + fId + ", fName=" + fName + ", fStatu=" + fStatu + ", fCtime=" + fCtime + ", fCuser="
				+ fCuser + ", fUptime=" + fUptime + ", fUpuser=" + fUpuser + "]";
	}

//	@Override
//	public String toString() {
//		return "Factory [fId=" + fId + ", fName=" + fName + ", fEquipment=" + fEquipment + ", fAdminstrator="
//				+ fAdminstrator + ", fProduct=" + fProduct + ", fStatu=" + fStatu + ", fCtime=" + fCtime + ", fCuser="
//				+ fCuser + ", fUptime=" + fUptime + ", fUpuser=" + fUpuser + "]";
//	}

	
	
	
	
}
