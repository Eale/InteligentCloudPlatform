package com.eale.inteligentcloud.domain;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
/**
 * 产品
 * @author Sail
 * 2018/11/20
 */
@Entity
public class ICPProduct {
    @Id
    @GeneratedValue
    /**
     * 产品id
     */
    private Long proFid;
    /**
     * 产品名称
     */
    private String proName;
    /**
     * 产品图片
     */
    private String proImgurl;
    /**
     * 产品描述
     */
    private String proDescribe;
    /**
     * 产品状态  存在[1]/失效[-1]
     */
    private Integer proState;
    /**
     * 创建时间
     */
    private Long proCtime;
    /**
     * 创建用户
     */
    private Long proCuser;
    /**
     * 修改时间
     */
    private Long  proUptime;
    /**
     * 修改用户
     */
    private Long proUpuser;
    
    /**
	 * 所属工厂
	 */
	@OneToOne
	private ICPFactory equipfactory;
	
	
	
    public ICPFactory getEquipfactory() {
		return equipfactory;
	}

	public void setEquipfactory(ICPFactory equipfactory) {
		this.equipfactory = equipfactory;
	}

	/**
     * 无参构造
     */
    public ICPProduct() {
    }

    public Long getProFid() {
        return proFid;
    }

    public void setProFid(Long proFid) {
        this.proFid = proFid;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProImgurl() {
        return proImgurl;
    }

    public void setProImgurl(String proImgurl) {
        this.proImgurl = proImgurl;
    }

    public String getProDescribe() {
        return proDescribe;
    }

    public void setProDescribe(String proDescribe) {
        this.proDescribe = proDescribe;
    }

    public Integer getProState() {
        return proState;
    }

    public void setProState(Integer proState) {
        this.proState = proState;
    }

    public Long getProCtime() {
        return proCtime;
    }

    public void setProCtime(Long proCtime) {
        this.proCtime = proCtime;
    }

    public Long getProCuser() {
        return proCuser;
    }

    public void setProCuser(Long proCuser) {
        this.proCuser = proCuser;
    }

    public Long getProUptime() {
        return proUptime;
    }

    public void setProUptime(Long proUptime) {
        this.proUptime = proUptime;
    }

    public Long getProUpuser() {
        return proUpuser;
    }

    public void setProUpuser(Long proUpuser) {
        this.proUpuser = proUpuser;
    }

	@Override
	public String toString() {
		return "ICPProduct [proFid=" + proFid + ", proName=" + proName + ", proImgurl=" + proImgurl + ", proDescribe="
				+ proDescribe + ", proState=" + proState + ", proCtime=" + proCtime + ", proCuser=" + proCuser
				+ ", proUptime=" + proUptime + ", proUpuser=" + proUpuser + ", equipfactory=" + equipfactory + "]";
	}
    
}

