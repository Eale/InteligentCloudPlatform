package com.eale.inteligentcloud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPAdminstrator;
import com.eale.inteligentcloud.domain.ICPFactory;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.AdminstratorService;
import com.eale.inteligentcloud.service.FactoryService;

/**
 * 管理员 控制器
 * @author Administrator
 *
 */
@RequestMapping("Adminstrator")
@RestController
public class AdminstratorController {
	
	@Autowired
	AdminstratorService adminstratorservice;
	
	@Autowired
	FactoryService factoryservice;
	
	/**
	 * 管理员登录
	 * @return
	 */
	@GetMapping("admin.do")
	public Object adminlogin(ICPAdminstrator admin) {
        Optional<ICPAdminstrator> opt = adminstratorservice.login(admin);
        if(opt.isPresent()) {
        	return "redirect:home.html";
        }else {
        	return "redircet:login.html";
        }      
    }
	
	/**
	 * 注册管理员
	 * @return
	 */
	@PostMapping("admin.do")
	public Object adminregister(@Validated ICPAdminstrator admin,
			BindingResult bindres) {
		
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
		
		Pageable page=PageRequest.of(0, 1);
		ICPFactory factory=new ICPFactory();
		factory.setfName(admin.getAdfactory().getfName());
		List<ICPFactory> list = factoryservice.findByConditionsPage(page, factory);
		if(list.size()>0) {
			admin.setAdCtime(System.currentTimeMillis());
			admin.setAdCuser((long) 1);// 测试用户
			admin.setAdUptime(System.currentTimeMillis());
			admin.setAdUpuser((long) 1);// 测试用户
			adminstratorservice.addAdmin(admin);
			return new Result(1);
		}
		return new Result(1, null, "请输入存在的工厂");

	}
	
	/**
	 * 删除管理员
	 * @return
	 */
	@DeleteMapping("admin.do")
	public Object admindelete(Long adis) {
		
		adminstratorservice.deleteAdmin(adis);
		return  new Result(1,null,"删除成功");
	}
	
	/**
	 * 批量删除
	 * @return
	 */
	@DeleteMapping("adminS.do")
	public Object adminSdelete(Long[] adids) {
		
		adminstratorservice.deleteAdmins(adids);
		return  new Result(1,null,"删除成功");
	}
	
	
	/**
	 * 修改管理员
	 * @return
	 */
	@PutMapping("admin.do")
	public Object adminUpdate(@Validated ICPAdminstrator admin,
			BindingResult bindres) {
		
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
		Optional<ICPAdminstrator> opt=adminstratorservice.findById(admin.getAdId());
		if(opt.isPresent()) {

            admin.setAdUptime(System.currentTimeMillis());
            admin.setAdUpuser((long) 1);//测试用户
			adminstratorservice.updateAdmin(admin);
			return new Result(1,null,"修改成功");
		}
		return new Result(1000,null,"修改失败");
		
		
	}
	
	/**
	 * 根据时间分页查询
	 * @return
	 */
	@GetMapping("admins.do")
	public Object getadmins(int page,int total,Long startTime,Long endTime) {
		Pageable pageable=PageRequest.of(page, total);
		List<ICPAdminstrator> list=adminstratorservice.findByPageTime(pageable,startTime,endTime);
		return new Result(1,list,null);
	}
	
	/**
	 * 根据时间状态分页查询
	 * @return
	 */
	@GetMapping("adminsByStatePage.do")
	public Object getadminsByTimePage(int page,int total,Long startTime,Long endTime) {
		int state=1;
		Pageable pageable=PageRequest.of(page, total);
		List<ICPAdminstrator> list=adminstratorservice.findByPageTimeState(pageable,startTime,endTime,state);
		return new Result(1,list,null);
	}
	
}
