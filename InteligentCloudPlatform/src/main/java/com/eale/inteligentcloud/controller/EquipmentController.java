package com.eale.inteligentcloud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPEquipment;
import com.eale.inteligentcloud.domain.ICPFactory;
import com.eale.inteligentcloud.domain.ICPProduct;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.EquipmentService;
import com.eale.inteligentcloud.service.FactoryService;
import com.eale.inteligentcloud.service.ProductService;

/**
 * 设备控制器
 * @author Administrator
 *
 */
@RequestMapping("equipment")
@RestController
public class EquipmentController {
	
	@Autowired
	EquipmentService equipmentService;
	
	@Autowired
	FactoryService factoryservice;
	
	@Autowired
	ProductService productservice;
	
	/**
	 * 分页查询
	 * @return
	 */
	@RequestMapping("list")
	public Object getList(int page,int total) {
		Sort sort = Sort.by("id");
		Pageable pageable = PageRequest.of(page, total,sort);
				
		List<ICPEquipment> list = equipmentService.findAllPage(pageable);
		return new Result(1, list, null);
	}
	
	/**
	 *  状态分页查询
	 * @return
	 */
	@RequestMapping("listStatePage")
	public Object getEquipmentByStatePage(int page,int total,int state) {
		Sort sort = Sort.by("id");
		Pageable pageable = PageRequest.of(page, total,sort);
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(state);
		List<ICPEquipment> list = equipmentService.findByExample(pageable, equipment);
		return new Result(1, list, null);
	}
	
	/**
	 * 根据状态产品查询设备---待机[1]
	 * @param page
	 * @param total
	 * @param state
	 * @return
	 */
	@RequestMapping("listStateproduct")
	public Object getEquipmentByStateproduct() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(1);
		List<ICPEquipment> list = equipmentService.findByCondtions(equipment);
		return new Result(1, list, null);
	}
	
	/**
	 *  状态查询  --待机[1]
	 * @return
	 */
	@RequestMapping("listState")
	public Object getEquipmentByState(int page,int total) {
		Sort sort = Sort.by("id");
		Pageable pageable = PageRequest.of(page, total,sort);
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(1);
		List<ICPEquipment> list = equipmentService.findByExample(pageable, equipment);
		return new Result(1, list, null);
	}
	
	/**
	 * 查看设备详情
	 * @return
	 */
	@RequestMapping("get")
	public Object getEquipment(long id) {
		Optional<ICPEquipment> opt = equipmentService.findById(id);
		if(opt.isPresent()) {
			return new Result(1, opt.get(),null);
		}else {
			return new Result(1, null,"设备没有找到");
		}
	}
	
	/**
	 * 录入设备
	 * @return
	 */
	@RequestMapping("add")
	public Object addEquipment(@Validated  ICPEquipment equipment,
			BindingResult bindres) {
		
		if(bindres.hasErrors()) {
			List<ObjectError> errors = bindres.getAllErrors();
			List<String> errorsMsg = new ArrayList<>();
			
			for (ObjectError error : errors) {
				errorsMsg.add(error.getDefaultMessage());
			}
			
			return new Result(1000, errorsMsg,"参数有误");
		}
        equipment.setEquipCtime(System.currentTimeMillis());
        equipment.setEquipCuser((long) 1);//测试用户
        equipment.setEquipUptime(System.currentTimeMillis());
        equipment.setEquipUpuser((long) 1);//测试用户
        
        List<ICPFactory> list = factoryservice.findByFactoryName(equipment.getEquipfactory().getfName());
        if(list.size()<1) {
        	return new Result(1, null, "请输入正确的工厂");
        }
        equipment.setEquipfactory(list.get(0));
        
        ICPProduct product=new ICPProduct();
		product.setProName(equipment.getEquipProduct().getProName());
        Pageable page=PageRequest.of(0, 1);
        List<ICPProduct> list2 = productservice.findByConditionsPage(page, product);
        if(list.size()<1) {
        	return new Result(1, null, "请输入正确的产品");
        }
        equipment.setEquipProduct(list2.get(0));
		equipmentService.addEquipment(equipment);
		return new Result(1);
	}
	
	/**
	 * 修改设备
	 * @return
	 */
	@RequestMapping("update")
	public Object updateEquipment(@Validated ICPEquipment equipment,
			BindingResult bindres) {
	
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}

        equipment.setEquipUptime(System.currentTimeMillis());
        equipment.setEquipUpuser((long) 1);//测试用户
		equipmentService.UpdateEquipment(equipment);
		return new Result(1);
	}
	
	/**
	 * 删除单个
	 * @return
	 */
	@DeleteMapping("equipment.do")
	public Object deleteequipment(long equipId) {
		equipmentService.deleteById(equipId);
		return new Result(1, null, "删除成功");
	}
	
	/**
	 * 批量删除
	 * @return
	 */
	@DeleteMapping("equipmentS.do")
	public Object deleteequipment(Long[] equipIds) {
		equipmentService.DeleteEquipment(equipIds);
		return new Result(1, null, "删除成功");
	}
	
	/**
	 * 时间分页查询
	 * @return
	 */
	@RequestMapping("gets.do")
	public Object getEquipment(int page,int total,long startTime,long endTime) {
		Pageable pageable=PageRequest.of(page, total);
		List<ICPEquipment> list = equipmentService.findByTimePage(pageable, startTime, endTime);
		return new Result(1, list, null);
	}
	
	/**
	 * 状态时间分页查询
	 * @return
	 */
	@RequestMapping("getsBystate.do")
	public Object getEquipmentbystate(int page,int total,long startTime,long endTime,int state) {
		Pageable pageable=PageRequest.of(page, total);
		List<ICPEquipment> list =equipmentService.findByTimePageState(pageable, startTime, endTime, state);
		return new Result(1, list, null);
	}
	
	/**
	 * 未启用[0]数量
	 * @return
	 */
	@GetMapping("bootNumber")
	public Object getbootNumber() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(0);
		int number = equipmentService.findEquipType(equipment);
		return new Result(1, number, null);
	}
	
	/**
	 * 待机[1]数量
	 * @return
	 */
	@GetMapping("standyNumber")
	public Object getstandyNumber() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(1);
		int number = equipmentService.findEquipType(equipment);
		return new Result(1, number, null);
	}
	/**
	 * 运行[2]数量
	 * @return
	 */
	@GetMapping("runNumber")
	public Object getrunNumber() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(2);
		int number = equipmentService.findEquipType(equipment);
		return new Result(1, number, null);
	}
	
	/**
	 * 停机[3]数量
	 * @return
	 */
	@GetMapping("downtimeNumber")
	public Object getdowntimeNumber() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(3);
		int number = equipmentService.findEquipType(equipment);
		return new Result(1, number, null);
	}
	
	/**
	 * 故障[4]数量
	 * @return
	 */
	@GetMapping("faultNumber")
	public Object getfaultNumber() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(4);
		int number = equipmentService.findEquipType(equipment);
		return new Result(1, number, null);
	}
	
	/**
	 * 失效[-1]数量
	 * @return
	 */
	@GetMapping("failureNumber")
	public Object getfailureNumber() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(-1);
		int number = equipmentService.findEquipType(equipment);
		return new Result(1, number, null);
	}
	
	/**
	 * 开机率
	 * @return
	 */
	@GetMapping("startRate")
	public Object getstartRate() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(1);
		int number = equipmentService.findEquipType(equipment);
		equipment.setEquipState(2);
		 number += equipmentService.findEquipType(equipment);
		 int totalNum=equipmentService.findNUmber();
		int rate=Math.round(100*number/totalNum) ;
		return new Result(1, rate, null);
	}
	/**
	 * 故障率
	 * @return
	 */
	@GetMapping("faultRate")
	public Object getfaultRate() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(4);
		int number = equipmentService.findEquipType(equipment);
		
		 int totalNum=equipmentService.findNUmber();
		int rate=Math.round(100*number/totalNum) ;
		System.out.println(rate);
		return new Result(1, rate, null);
	}
	/**
	 * 运行率
	 * @return
	 */
	@GetMapping("runNumberRate")
	public Object getrunNumberRate() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(2);
		int number = equipmentService.findEquipType(equipment);
		
		 int totalNum=equipmentService.findNUmber();
		int rate=Math.round(100*number/totalNum) ;
		
		return new Result(1, rate, null);
	}
	/**
	 * 综合率
	 * @return
	 */
	@GetMapping("syntheticalRate")
	public Object getsyntheticalRate() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(2);
		int number = equipmentService.findEquipType(equipment);
		equipment.setEquipState(1);
		 number += equipmentService.findEquipType(equipment);
		
		 int totalNum=equipmentService.findNUmber();
		int rate=Math.round(100*number/totalNum) ;
		
		return new Result(1, rate, null);
	}
}
