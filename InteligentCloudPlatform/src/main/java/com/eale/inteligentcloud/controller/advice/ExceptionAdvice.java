package com.eale.inteligentcloud.controller.advice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.exception.ParamValidatedException;


/**
 * 统一异常处理
 * @author Administrator
 *
 */
@ControllerAdvice
public class ExceptionAdvice  {
	
	
	
	
	/**
	 * 处理自定义参数绑定异常
	 * @return
	 */
	@ResponseBody
	@ExceptionHandler(ParamValidatedException.class)
	public Object doParamValidatedException(List<ObjectError> ex) {
		
				
			List<ObjectError> errors = ((ParamValidatedException) ex).getAllErrors();
			List<String> errorsMsg = new ArrayList<>();
			
			for (ObjectError error : errors) {
				errorsMsg.add(error.getDefaultMessage());
			}
			
			return new Result(1000, errorsMsg,"参数有误");
	
	}
	
	/**
	 * 处理自定义异常
	 * @return
	 */
	@ResponseBody
	@ExceptionHandler(MyException.class)
	public Object doMyException() {
		return new Result(-100,null,null);
	}
	
	
	/**
	 * 处理所有异常
	 * @return
	 */
//	@ResponseBody
//	@ExceptionHandler(Exception.class)
//	public Object doException() {
//		return new Result(-100,null,null);
//	}

	
	
}
