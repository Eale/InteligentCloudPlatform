package com.eale.inteligentcloud.controller;


import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPFactory;
import com.eale.inteligentcloud.domain.ICPProduct;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.FactoryService;
import com.eale.inteligentcloud.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

/**
 * @author Sail
 * 2018/11/21
 */
@RequestMapping("product")
@RestController
public class ProductController {
    @Autowired
    ProductService productservice;
    
    @Autowired
    FactoryService factoryservice;

    /**
     * 分页查询 全部
     *
     * @return
     */
    @GetMapping("product.do")
    public Object getAllPage(int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        List<ICPProduct> dbProducts = productservice.findAllPage(pageable);
        if (dbProducts.size() > 0) {
            return new Result(1, dbProducts, null);
        } else {
            return new Result(1, null, "产品没有找到");
        }
    }
    
    /**
     * 状态查询(未删除)
     * @return
     */
    @GetMapping("productAllState.do")
    public Object getAllState() {
    	ICPProduct product=new ICPProduct();
        product.setProState(1);
        List<ICPProduct> dbProducts = productservice.findAllState(product);
        if (dbProducts.size() > 0) {
            return new Result(1, dbProducts, null);
        } else {
            return new Result(1, null, "产品没有找到");
        }
    }
    
    /**
     * 根据名字查询
     * @param proName
     * @return
     */
    @GetMapping("getproductByname.do")
    public Object getproductByname(String proName) {
    	Optional<ICPProduct> opt = productservice.findByproName(proName);
    	if(opt.isPresent()) {
    		return new Result(1,opt.get(), null);
    	}
    	return new Result(1, null, "输入有效的产品名");
    }
    
    /**
     * 按条件分页查询product(未删除状态 1)proState
     *
     * @return
     */
    @GetMapping("searchProductCondition.do")
    public Object getConditionsPage(int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        ICPProduct product=new ICPProduct();
        product.setProState(1);
        List<ICPProduct> dbProducts = productservice.findByConditionsPage(pageable, product);
        if (dbProducts.size() > 0) {
            return new Result(1, dbProducts, null);
        } else {
            return new Result(1, null, "产品没有找到");
        }
    }

    /**
     * 按fid查询
     *
     * @return
     */
    @GetMapping("searchProduct.do")
    public Object getProduct(Long proFid) {
        Optional<ICPProduct> opt = productservice.findById(proFid);
        if (opt.isPresent()) {
            return new Result(1, opt, null);
        } else {
            return new Result(1, null, "产品没有找到");
        }
    }

    /**
     * 按产品创建时间范围查询
     *
     * @return
     */
    @GetMapping("searchProductTime.do")
    public Object getConditionsPage(Long beginTime, Long endTime, int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        List<ICPProduct> dbProducts = productservice.findConditionsPage(beginTime, endTime, pageable);
        if (dbProducts.size() > 0) {
            return new Result(1, dbProducts, null);
        } else {
            return new Result(1, null, "产品没有找到");
        }
    }


    /**
     * 增加Product
     *
     * @return
     */
    @PostMapping("product.do")
    public Object addProduct(@Validated ICPProduct product, BindingResult bindres) {
        if (bindres.hasErrors()) {
            throw new ParamValidatedException(bindres.getAllErrors());
        }
        
        List<ICPFactory> factory = factoryservice.findByFactoryName(product.getEquipfactory().getfName());
        if(factory.size()<1){
            return new Result(1, null, "数据库错误");
        }
        product.setEquipfactory(factory.get(0));
        
        product.setProCtime(System.currentTimeMillis());
        product.setProCuser((long) 1);//测试用户
        product.setProUptime(System.currentTimeMillis());
        product.setProUpuser((long) 1);//测试用户
        // 存入数据库
        productservice.addProduct(product);
        return new Result(1, null, null);
    }

    /**
     * 修改Product
     *
     * @return
     */
    @PutMapping("product.do")
    public Object updateProduct(ICPProduct product, BindingResult bindres) {
        if (bindres.hasErrors()) {
            throw new ParamValidatedException(bindres.getAllErrors());
        }
        product.setProUptime(System.currentTimeMillis());
        product.setProUpuser((long) 1);//测试用户
        productservice.updateProduct(product);
        return new Result(1);
    }


    /**
     * 删除Product
     *
     * @return
     */
    @DeleteMapping("product.do")
    public Object delProduct(Long proFid) {
    	
        productservice.deleteProduct(proFid);
        return new Result(1);

    }

    /**
     * 批量删除Product
     *
     * @return
     */
    @DeleteMapping("productS.do")
    public Object delProducts(@RequestParam(value = "proFids[]") Long[] proFids) {
    	productservice.deleteProducts(proFids);
		return new Result(1, null, "删除成功");
    }

}
