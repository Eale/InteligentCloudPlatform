package com.eale.inteligentcloud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPFactory;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.FactoryService;

@RequestMapping("factory")
@RestController
public class FactoryController {
	
	@Autowired
	FactoryService factoryservice;
	
	/**
	 * 查询单个工厂
	 * @return
	 */
	@GetMapping("factory.do")
	public Object getfactory(Long fId) {
        Optional<ICPFactory> opt = factoryservice.findById(fId);
        if(opt.isPresent()) {
        	return new Result(1,opt.get(),null);
        }
        return new Result(1,null,"没查到该工厂");
    }
	
	/**
	 * 注册工厂
	 * @return
	 */
	@PostMapping("factory.do")
	public Object registerfactory(@Validated ICPFactory factory,
			BindingResult bindres) {
		
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
        factory.setfCtime(System.currentTimeMillis());
        factory.setfCuser((long) 1);//测试用户
        factory.setfUptime(System.currentTimeMillis());
        factory.setfUpuser((long) 1);//测试用户
		factoryservice.addFactory(factory);
		return new Result(1);

	}
	
	/**
	 * 删除工厂
	 * @return
	 */
	@DeleteMapping("factory.do")
	public Object deletefactory(Long fid) {
		
		factoryservice.deleteFactory(fid);
		return  new Result(1,null,"删除成功");
	}
	
	/**
	 * 批量删除
	 * @return
	 */
	@DeleteMapping("factoryS.do")
	public Object deletefactoryS(Long[] fids) {	
		
		factoryservice.deleteFactorys(fids);
		return  new Result(1,null,"删除成功");
	}
	
	
	/**
	 * 修改工厂
	 * @return
	 */
	@PutMapping("Upfactory.do")
	public Object factoryUpdate(@Validated ICPFactory factory,
			BindingResult bindres) {
		
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
		Optional<ICPFactory> opt=factoryservice.findById(factory.getfId());
		if(opt.isPresent()) {

            factory.setfUptime(System.currentTimeMillis());
            factory.setfUpuser((long) 1);//测试用户
			factoryservice.updateFactory(factory);
			return new Result(1,null,"修改成功");
		}
		return new Result(1000,null,"修改失败");	
	}
	
	/**
	 * 全部分页查询
	 * @param page
	 * @param total
	 * @return
	 */
	@GetMapping("factorys.do")
	public Object getfactorys(int page,int total) {
		Pageable pageable=PageRequest.of(page, total);
		List<ICPFactory> factorys=factoryservice.findAll(pageable);
		return new Result(1,factorys,null);
	}
	
	/**
	 * 按状态分页查询(未删除)
	 * @param page
	 * @param total
	 * @return
	 */
	@GetMapping("factorysByStatePage.do")
	public Object getfactorysByStatePage(int page,int total) {
		ICPFactory factory=new ICPFactory();
		factory.setfStatu(1);
		
		Pageable pageable=PageRequest.of(page, total);
		List<ICPFactory> factorys=factoryservice.findByConditionsPage(pageable, factory);
		return new Result(1,factorys,null);
	}
	
	
	/**
	 * 按状态查询(未删除)
	 * @param page
	 * @param total
	 * @return
	 */
	@GetMapping("factorysByState.do")
	public Object getfactorysByState() {
		ICPFactory factory=new ICPFactory();
		factory.setfStatu(1);
		List<ICPFactory> factorys=factoryservice.findAllByState(factory);
		return new Result(1,factorys,null);
	}
	
	/**
	 * 全部按时间分页查询
	 * @return
	 */
	@GetMapping("factorysByTime.do")
	public Object getfactorysByTime(int page,int total,long startTime,long endTime) {
		Pageable pageable=PageRequest.of(page, total);
		List<ICPFactory> factorys=factoryservice.findByPageTime(pageable,startTime,endTime);
		return new Result(1,factorys,null);
	}
	
	/**
	 * 按状态询时间分页查询(未删除)
	 * @param page
	 * @param total
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@GetMapping("factorysByStateTime.do")
	public Object getfactorysByState(int page,int total,long startTime,long endTime) {
		int state=1;
		Pageable pageable=PageRequest.of(page, total);
		List<ICPFactory> factorys=factoryservice.findByPageTimeByState(pageable,startTime,endTime,state);
		return new Result(1,factorys,null);
	}
}
