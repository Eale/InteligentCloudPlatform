package com.eale.inteligentcloud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPFollow;
import com.eale.inteligentcloud.domain.ICPOrder;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.FollowService;
import com.eale.inteligentcloud.service.OrderService;

@RequestMapping("follow")
@RestController
public class FollowController {
	
	@Autowired
	FollowService followservice;
	
	@Autowired
	OrderService orderservice;
	
	/**
	 * 添加报工单
	 * @return
	 */
	@PostMapping("follow.do")
	public Object addfollow(@Validated ICPFollow follow,
			BindingResult bindres) {
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
		List<ICPOrder> list = orderservice.findByExample(follow.getFollowOrder());
		if(list.size()<1) {
			return new Result(1, null, "订单有误");
		}
		follow.setFollowOrder(list.get(0));
		follow.setFollowState(-1);
        follow.setFollowCtime(System.currentTimeMillis());
        follow.setFollowCuser((long) 1);//测试用户
		followservice.addFollow(follow);
		return new Result(1);		
	}
	
	/**
	 * 查询未报工工单
	 * @return
	 */
	@GetMapping("Unfollow.do")
	public Object getUnfollows(int page,int total) {
		Pageable pageable=PageRequest.of(page, total);
		int followState=-1;
		List<ICPFollow> dispatches=followservice.findByStatePage(pageable,followState);
		return new Result(1, dispatches, null);	
	}
	
	/**
	 * 查询已报工工单
	 * @return
	 */
	@GetMapping("follow.do")
	public Object getfollows(int page,int total) {
		Pageable pageable=PageRequest.of(page, total);
		int followState=1;
		List<ICPFollow> dispatches=followservice.findByStatePage(pageable,followState);
		return new Result(1, dispatches, null);	
	}
	
	/**
	 * 报工单进行报工
	 * @return
	 */
	@PutMapping("follow.do")
	public Object updatefollow(@Validated ICPFollow follow,
			BindingResult bindres) {
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
		followservice.submitRepairOrder(follow);
		return new Result(1, null, "报工成功");
	}
	
	/**
	 * 查询一个订单里面产品的完成数量
	 * @return
	 */
	@GetMapping("followsNUm.do")
	public Object getfollowsNum(Long oId) {
		Optional<ICPOrder> opt = orderservice.findById(oId);
		if(opt.isPresent()) {
			int qualifyNum=followservice.findbyFollowNumber(oId);
			return new Result(1,qualifyNum,null);
		}
		return new Result(1, null, "订单有误");
	}
	
	
}
