package com.eale.inteligentcloud.controller;


import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPOrder;
import com.eale.inteligentcloud.domain.ICPProductionPlan;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.OrderService;
import com.eale.inteligentcloud.service.ProductionPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


/**
 * @author Sail
 * 2018/11/21
 */
@RequestMapping("productionPlan")
@RestController
public class ProductionPlanController {
    
	@Autowired
    ProductionPlanService service;
    
    @Autowired
    OrderService orderservice;
    
    /**
     * 增加ProductionPlan
     *
     * @return
     */
    @PostMapping("productionPlan.do")
    public Object addProductionPlan(@Validated ICPProductionPlan productionPlan, BindingResult bindres) {
        if (bindres.hasErrors()) {
            throw new ParamValidatedException(bindres.getAllErrors());
        }
        
        List<ICPOrder> list = orderservice.findByExample(productionPlan.getPlanOrder());
        if(list.size()<1) {
        	return new Result(1,null,"订单有误");
        }
        productionPlan.setPlanOrder(list.get(0));
        productionPlan.setPlanCtime(System.currentTimeMillis());
        productionPlan.setPlanCuser((long) 1);//测试用户
        productionPlan.setPlanUptime(System.currentTimeMillis());
        productionPlan.setPlanUpuser((long) 1);//测试用户
        //存入数据库
        service.addProductionPlan(productionPlan);
        return new Result(1);
    }
    
    
    /**
     * 分页查询 全部
     *
     * @return
     */
    @GetMapping("productionPlan.do")
    public Object getAllPage(int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        List<ICPProductionPlan> dbProductionPlans = service.findAllPage(pageable);
        if(dbProductionPlans.size()>0){
            return new Result(1, dbProductionPlans, null);
        }else {
            return new Result(1, null, "计划没有找到");
        }
    }
    
    /**
     * 状态分页查询    未启动[1]
     * @param page
     * @param total
     * @return
     */
    @GetMapping("produtcByStatePage.do")
    public Object getprodutcByStatePage(int page, int total) {
    	Pageable pageable = PageRequest.of(page, total);
    	ICPProductionPlan productionPlan=new ICPProductionPlan();
    	productionPlan.setPlanState(1);
        List<ICPProductionPlan> dbProductionPlans = service.findByConditionsPage(pageable, productionPlan);
        if(dbProductionPlans.size()>0){
            return new Result(1, dbProductionPlans, null);
        }else {
            return new Result(1, null, "计划没有找到");
        }
    }
    
    /**
     * 状态分页查询    已启动[2]
     * @param page
     * @param total
     * @return
     */
    @GetMapping("produtcByStatePagelaunched.do")
    public Object getprodutcByStatePagelaunched(int page, int total) {
    	Pageable pageable = PageRequest.of(page, total);
    	ICPProductionPlan productionPlan=new ICPProductionPlan();
    	productionPlan.setPlanState(2);
        List<ICPProductionPlan> dbProductionPlans = service.findByConditionsPage(pageable, productionPlan);
        if(dbProductionPlans.size()>0){
            return new Result(1, dbProductionPlans, null);
        }else {
            return new Result(1, null, "计划没有找到");
        }
    }
    
    /**
     * 状态分页查询    已完成[3]
     * @param page
     * @param total
     * @return
     */
    @GetMapping("produtcByStatePagefinished.do")
    public Object getprodutcByStatePagefinished(int page, int total) {
    	Pageable pageable = PageRequest.of(page, total);
    	ICPProductionPlan productionPlan=new ICPProductionPlan();
    	productionPlan.setPlanState(3);
        List<ICPProductionPlan> dbProductionPlans = service.findByConditionsPage(pageable, productionPlan);
        if(dbProductionPlans.size()>0){
            return new Result(1, dbProductionPlans, null);
        }else {
            return new Result(1, null, "计划没有找到");
        }
    }
    

    /**
     * 按条件分页查询productionPlan 
     *
     * @return
     */
    @GetMapping("searchProductionPlanConsition.do")
    public Object getConditionsPage(ICPProductionPlan productionPlan, int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        List<ICPProductionPlan> dbProductionPlans = service.findByConditionsPage(pageable, productionPlan);
        if(dbProductionPlans.size()>0){
            return new Result(1, dbProductionPlans, null);
        }else {
            return new Result(1, null, "计划没有找到");
        }
    }

    /**
     * 按fid查询--查看订单详情
     *
     * @return
     */
    @GetMapping("searchProductionPlan.do")
    public Object getProductionPlan(Long planFid) {
        Optional<ICPProductionPlan> opt = service.findById(planFid);
        if (opt.isPresent()) {
            return new Result(1, opt, null);
        } else {
            return new Result(1, null, "计划没有找到");
        }

    }

    /**
     * 按产品创建时间范围查询
     *
     * @return
     */
    @GetMapping("searchProductionPlanTime.do")
    public Object getConditionsPage(Long beginTime, Long endTime, int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        List<ICPProductionPlan> dbProductionPlans = service.findConditionsPage(beginTime, endTime, pageable);
        if(dbProductionPlans.size()>0){
            return new Result(1, dbProductionPlans, null);
        }else {
            return new Result(1, null, "计划没有找到");
        }
    }

    /**
     * 按订单查询
     *
     * @return
     */
    @GetMapping("searchOrderPage.do")
    public Object getOrderPage(ICPOrder order, int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        List<ICPProductionPlan> dbProductionPlans = service.findOrderPage(order, pageable);
        if(dbProductionPlans.size()>0){
            return new Result(1, dbProductionPlans, null);
        }else {
            return new Result(1, null, "计划没有找到");
        }
    }

    

    /**
     * 修改ProductionPlan
     *
     * @return
     */
    @PutMapping("productionPlan.do")
    public Object updateProductionPlan(@Validated ICPProductionPlan productionPlan, BindingResult bindres) {

        if (bindres.hasErrors()) {
            throw new ParamValidatedException(bindres.getAllErrors());
        }
        
        productionPlan.setPlanUptime(System.currentTimeMillis());
        productionPlan.setPlanUpuser((long) 1);//测试用户
        service.updateProductionPlan(productionPlan);
        return new Result(1);

    }

    /**
     * 启动生产计划
     *
     */
    @PutMapping("launchProductionPlan.do")
    public Object launchProductionPlans(Long planId) {

        service.launchProductionPlans(planId);
        return new Result(1);

    }

    /**
     * 删除ProductionPlan
     *
     * @return
     */
    @DeleteMapping("productionPlan.do")
    public Object delProductionPlan(Long planFid) {
       

        service.deleteProductionPlan(planFid);
        return new Result(1);

    }

    /**
     * 批量删除ProductionPlan
     *
     * @return
     */
    @DeleteMapping("productionPlanS.do")
    public Object delProductionPlans(Long[] planFids) {

        service.deleteProductionPlans(planFids);
        return new Result(1);
    }

}
