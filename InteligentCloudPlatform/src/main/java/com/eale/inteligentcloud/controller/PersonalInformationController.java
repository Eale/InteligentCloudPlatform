package com.eale.inteligentcloud.controller;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPAdminstrator;
import com.eale.inteligentcloud.domain.ICPPersonalInformation;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.AdminstratorService;
import com.eale.inteligentcloud.service.PersonalInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author Sail
 * 2018/11/22
 */
@RequestMapping("PersonalInformation")
@RestController
public class PersonalInformationController {

    @Autowired
    PersonalInformationService service;
    
    @Autowired
    AdminstratorService adminstratorservice;
    
    /**
     * 按条件分页查询personalInformation
     *
     * @return
     */
    @GetMapping("searchPersonalInformation.do")
    public Object getConditionsPage(ICPPersonalInformation personalInformation, int page, int total) {
        Pageable pageable = PageRequest.of(page, total);
        List<ICPPersonalInformation> dbPersonalInformation = service.findByConditionsPage(pageable, personalInformation);
        if (dbPersonalInformation.size() > 0) {
            return new Result(1, dbPersonalInformation, null);
        } else {
            return new Result(1, null, "没有找到");
        }
    }

    /**
     * 按fid查询personalInformation
     */
    @GetMapping("searchPersonal.do")
    public Object getConditionsPage(Long perFid) {
        Optional<ICPPersonalInformation> opt = service.findById(perFid);
        if (opt.isPresent()) {
            return new Result(1, opt, null);
        } else {
            return new Result(1, null, "没有找到");
        }
    }
    
    /**
     * 按管理员ID查询personalInformation
     */
    @GetMapping("getPersonal.do")
    public Object getadminPersonalInformation(Long adId) {
        Optional<ICPPersonalInformation> opt = service.findByAdminId(adId);
        if (opt.isPresent()) {
            return new Result(1, opt, null);
        } else {
            return new Result(1, null, "没有找到");
        }
    }

    /**
     * 增加PersonalInformation
     *
     * @return
     */
    @PostMapping("personalInformation.do")
    public Object addPersonalInformation(@Validated ICPPersonalInformation personalInformation, BindingResult bindres) {
        if (bindres.hasErrors()) {
            throw new ParamValidatedException(bindres.getAllErrors());
        }
        Optional<ICPAdminstrator> opt = adminstratorservice.findById(personalInformation.getPerId());
        if(!opt.isPresent()) {
        	return new Result(1, null, "管理员有误");
        }
        
        personalInformation.setPerCtime(System.currentTimeMillis());
        personalInformation.setPerCuser((long) 1);//测试用户
        //存入数据库
        service.addPersonalInformation(personalInformation);
        return new Result(1);

    }

    /**
     * 修改PersonalInformation
     *
     * @return
     */
    @PutMapping("personalInformation.do")
    public Object updatePersonalInformation(@Validated ICPPersonalInformation personalInformation, BindingResult bindres) {
        if (bindres.hasErrors()) {
            throw new ParamValidatedException(bindres.getAllErrors());
        }
        personalInformation.setPerUptime(System.currentTimeMillis());
        personalInformation.setPerUpuser((long) 1);//测试用户
        service.updatePersonalInformation(personalInformation);
        return new Result(1);
    }

    /**
     * 删除PersonalInformation
     *
     * @return
     */
    @DeleteMapping("personalInformation.do")
    public Object delPersonalInformation(Long proFid) {

        service.deletePersonalInformation(proFid);
        return new Result(1);
    }

    /**
     * 批量删除PersonalInformation
     *
     * @return
     */
    @DeleteMapping("personalInformationS.do")
    public Object delPersonalInformations(Long[] proFids) {
       
        service.deletePersonalInformations(proFids);
        return new Result(1);
    }

}
