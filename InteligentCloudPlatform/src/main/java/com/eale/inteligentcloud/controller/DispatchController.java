package com.eale.inteligentcloud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPDispatch;
import com.eale.inteligentcloud.domain.ICPEquipment;
	import com.eale.inteligentcloud.domain.ICPProductionPlan;
import com.eale.inteligentcloud.exception.ParamValidatedException;
import com.eale.inteligentcloud.service.DispatchService;
import com.eale.inteligentcloud.service.EquipmentService;
import com.eale.inteligentcloud.service.OrderService;
import com.eale.inteligentcloud.service.ProductionPlanService;

/**
 * 生产调度--工单 控制器
 * @author Administrator
 *
 */
@RequestMapping("dispatch")
@RestController
public class DispatchController {
	
	@Autowired
	DispatchService dispatchservice;
	
	@Autowired
	ProductionPlanService productionplanservice;
	
	@Autowired
	EquipmentService equipmentservice;
	
	@Autowired
	OrderService orderservice;
	
	
	/**
	 * 添加工单
	 * @return
	 */
	@PostMapping("dispatch.do")
	public Object adddispatch(@Validated ICPDispatch dispatch,
			BindingResult bindres
			) {
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
        dispatch.setDispCtime(System.currentTimeMillis());
        dispatch.setDispCuser((long) 1);//测试用户
        dispatch.setDispUptime(System.currentTimeMillis());
        dispatch.setDispUpuser((long) 1);//测试用户
        
        Optional<ICPProductionPlan> opt = productionplanservice.findById(dispatch.getDisProductionPlan().getPlanId());
        if(!opt.isPresent()) {
        	return new Result(1, null, "请输入正确的生产计划");
        }
        dispatch.setDisProductionPlan(opt.get());
        dispatch.setDispState(1);
		dispatchservice.adddispatch(dispatch);
		return new Result(1);		
	}
	
	/**
	 * 启动工单
	 * @return
	 */
	@PutMapping("launchdispatch.do")
	public Object launchdispatch(@Validated ICPDispatch dispatch,BindingResult bindres) {
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}
		
		Optional<ICPProductionPlan> opt = productionplanservice.findById(dispatch.getDisProductionPlan().getPlanId());	
		if(!opt.isPresent()) {
			return new Result(1,null,"请输入正确的生产计划");
		}
		
		List<ICPEquipment> list = equipmentservice.findBySequence(dispatch.getDisequipment().getEquipSequence());
		if(list.size()<1) {		
			return new Result(1,null,"请输入正确的设备");
		}
		
		dispatch.setDisProductionPlan(opt.get());
		
		dispatch.setDisequipment(list.get(0));
		
		dispatchservice.launch(dispatch);
		return new Result(1);
	}
	
	
	/**
	 * 修改工单
	 * @returnl
	 */
	@PutMapping("dispatch.do")
	public Object updatedispatch(@Validated ICPDispatch dispatch,
			BindingResult bindres) {
		if(bindres.hasErrors()) {
			throw new ParamValidatedException(bindres.getAllErrors());
		}

        dispatch.setDispUptime(System.currentTimeMillis());
        dispatch.setDispUpuser((long) 1);//测试用户
		dispatchservice.updatedispatch(dispatch);
		return new Result(1);
	}
	/**
	 * 查询单个生产调度--工单
	 * @return
	 */
	@GetMapping("dispatch.do")
	public Object getdispatch(Long disFid) {
		Optional<ICPDispatch> opt=dispatchservice.findById(disFid);
		if(opt.isPresent()) {
			return new Result(1, opt.get(),null);
		}
		return new Result(1,null,"不存在工单");
	}
	
	/**
	 * 删除工单
	 * @return
	 */
	@DeleteMapping("follow.do")
	public Object deletedispatch(long disFid) {
		dispatchservice.deletedispatch(disFid);
		return new Result(1);
	}
	
	/**
	 * 批量删除
	 * @return
	 */
	@DeleteMapping("followS.do")
	public Object deletedispatches(Long[] dispFids) {
		dispatchservice.deletedispatches(dispFids);
		return new Result(1);
	}
	
	/**
	 * 分页查询未启动工单
	 * @return
	 */
	@GetMapping("Undispatch.do")
	public Object getUndispatches(int page,int total) {
		Pageable pageable=PageRequest.of(page, total);
		int dispState=1;
		List<ICPDispatch> dispatches=dispatchservice.findByStatePage(pageable,dispState);
		return new Result(1, dispatches, null);	
	}
	
	/**
	 * 分页查询已处理工单
	 * @return
	 */
	@GetMapping("dispatchs.do")
	public Object getdispatches(int page,int total) {
		Pageable pageable=PageRequest.of(page, total);
		int dispState=2;
		
		List<ICPDispatch> dispatches=dispatchservice.findByStatePage(pageable,dispState);
		return new Result(1, dispatches, null);	
	}
	
	/**
	 * 按时间分页查询工单
	 * @return
	 */
	@GetMapping("dispatchsByTime.do")
	public Object getdispatches(int page,int total,long startTime,long endTime) {
		Pageable pageable=PageRequest.of(page, total);
		List<ICPDispatch> dispatches=dispatchservice.findByTime(pageable, startTime, endTime);
		return new Result(1, dispatches, null);	
	}
	
}
