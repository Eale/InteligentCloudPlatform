package com.eale.inteligentcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InteligentCloudPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(InteligentCloudPlatformApplication.class, args);
	}
}
