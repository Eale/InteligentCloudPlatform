//package com.eale.inteligentcloud.aspect;
//
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.AfterThrowing;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//
///**
// * Aop切面
// * @author Administrator
// *
// */
//@Aspect
//@Component
//public class MyAspect {
//	//LoggerFactory日志工厂,getLogger(MyAspect.class)设置上下文
//	private final static Logger logger = LoggerFactory.getLogger(MyAspect.class);
//	
//	//切入点
//	//在中间方法上写拦截，在其它地方按方法调用类似使用，避免重复代码
//    @Pointcut("execution(public * com.eale.inteligentcloud.service.impl.AdminstratorServiceImpl.*(..))")
//    public void log() {
//    }
//    
//    //前置通知
//	@Before("log()")
//	public void doBefore(JoinPoint joinPoint) {
//		Object[] args=joinPoint.getArgs();
//		
//		if(Integer.valueOf(args[0].toString())>100) {
//			logger.debug("目标参数"+args[0]);
//			
//		}
//		
//	}
//	
//	//环绕通知
//	@Around("log()")
//	private Object dochain(ProceedingJoinPoint point) throws Throwable {
//		Object[] args = point.getArgs();
//		int i=Integer.valueOf(args[0].toString());
//			if(i>100) args[0]=99;	
//
//			if(i>0) {
//				//让方法继续执行
//				
//				
//				return point.proceed(args);
//			}
//
//		return null;
//	}
//	
//	//log() 这个就是上面log()方法,简化我们的切入点
//	//也意味着每一种通知都可以是独立的切入点
//	@After("log()")
//	public void doAfter() {
//		logger.info("后置通知没事干");
//	}
//	
//	//返回值
//	@AfterReturning(returning = "object", pointcut = "log()")
//    public void doAfterReturning(Object object) {
//        logger.info("response={}", null!=object?object.toString():"null");
//    }
//	
//	//异常切入点
//	@AfterThrowing(throwing="throwable",pointcut="log()")
//	public void doAfterThrowing(Throwable throwable) {
//		logger.error("切入点拦截到异常:"+throwable.getMessage());
//	}
//}
