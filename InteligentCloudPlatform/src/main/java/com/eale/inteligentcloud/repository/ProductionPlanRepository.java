package com.eale.inteligentcloud.repository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.eale.inteligentcloud.domain.ICPProductionPlan;

/**
 * repository是mapper的升级版,<ProductionPlan--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface ProductionPlanRepository extends JpaRepository<ICPProductionPlan, Long> {
	@Query(value="SELECT * FROM icpproduction_plan WHERE  plan_ctime between ?1 AND ?2  ",nativeQuery=true)
	List<ICPProductionPlan> findByPageAndTime(Long startTime,Long endTime,Pageable page);
}
