package com.eale.inteligentcloud.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.eale.inteligentcloud.domain.ICPFactory;
import java.lang.String;

/**
 *  repository是mapper的升级版,<Factory--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface FactoryRepository extends JpaRepository<ICPFactory, Long> {
	
	List<ICPFactory> findByFName(String fname);
	
	@Query(value="SELECT * FROM icpfactory WHERE  f_ctime between ?1 AND ?2  ",nativeQuery=true)
	List<ICPFactory> findByPageTime(long startTime, long endTime,Pageable pageable);
	
	@Query(value="SELECT * FROM icpfactory WHERE  f_ctime between ?1 AND ?2 AND f_statu=?3 ",nativeQuery=true)
	List<ICPFactory> findByPageTimeState(long startTime, long endTime,int state,Pageable pageable);

}
