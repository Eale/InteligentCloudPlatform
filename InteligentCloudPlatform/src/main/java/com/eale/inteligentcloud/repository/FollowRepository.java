package com.eale.inteligentcloud.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.eale.inteligentcloud.domain.ICPFollow;

/**
 * repository是mapper的升级版,<Follow--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface FollowRepository extends JpaRepository<ICPFollow, Long> {
	
	@Query(value="SELECT * FROM icpfollow WHERE  follow_strat_time between ?1 AND ?2  ",nativeQuery=true)
	List<ICPFollow> findByPageTime(long startTime, long endTime,Pageable pageable);
	
}
