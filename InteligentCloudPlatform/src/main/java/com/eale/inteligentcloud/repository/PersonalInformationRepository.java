package com.eale.inteligentcloud.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eale.inteligentcloud.domain.ICPAdminstrator;
import com.eale.inteligentcloud.domain.ICPPersonalInformation;

/**
 * repository是mapper的升级版,<PersonalInformation--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface PersonalInformationRepository extends JpaRepository<ICPPersonalInformation, Long> {
	Optional<ICPPersonalInformation> findByAdmins(ICPAdminstrator admin);
}
