package com.eale.inteligentcloud.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.eale.inteligentcloud.domain.ICPDispatch;

/**
 * repository是mapper的升级版,<Dispatch--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface DispatchRepository extends JpaRepository<ICPDispatch, Long> {
	@Query(value="SELECT * FROM icpdispatch WHERE  disp_ctime between ?1 AND ?2  ",nativeQuery=true)
	List<ICPDispatch> findByTime(Long startTime, Long endTime,Pageable page);
	
	@Query(value="	select * from icpdispatch where disp_fid in (select disp_fid from icpfollow where disp_state = ?1)"  ,nativeQuery=true)
	List<ICPDispatch> findByStatePage(int dispState,Pageable pageable);
}
