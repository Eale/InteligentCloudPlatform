package com.eale.inteligentcloud.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.eale.inteligentcloud.domain.ICPAdminstrator;

/**
 *  repository是mapper的升级版,<Adminstrator--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface AdminstratorRepository extends JpaRepository<ICPAdminstrator, Long> {

	Optional<ICPAdminstrator> findByAdNumber(String adNumber);
	
	@Query(value="SELECT * FROM icpadminstrator WHERE  ad_ctime between ?1 AND ?2  ",nativeQuery=true)
	List<ICPAdminstrator> findByPageTime(Long startTime, Long endTime,Pageable pageable);
	
	@Query(value="SELECT * FROM icpadminstrator WHERE  ad_ctime between ?1 AND ?2  AND ad_delete=?3",nativeQuery=true)
	List<ICPAdminstrator> findByPageTimeState(Long startTime, Long endTime,Integer state,Pageable pageable);

}
