
package com.eale.inteligentcloud.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.eale.inteligentcloud.domain.ICPOrder;

/**
 * repository是mapper的升级版,<Order--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface OrderRepository extends JpaRepository<ICPOrder, Long> {
	
	List<ICPOrder> findByoStateNotIn(List<Integer> state,Pageable pageable);
	
	@Query(value="select * from icporder where o_deadline between ?1 and ?2",nativeQuery=true)
	List<ICPOrder> findByoState(Long beginTime, Long endTime, Pageable page);
}
