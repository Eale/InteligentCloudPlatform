package com.eale.inteligentcloud.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.eale.inteligentcloud.domain.ICPEquipment;

/**
 * repository是mapper的升级版,<Equipment--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface EquipmentRepository extends JpaRepository<ICPEquipment, Long> {
	
	List<ICPEquipment> findByequipSequence(String equipSequence);
	
	@Query(value="SELECT * FROM icpequipment WHERE  equip_ctime between ?1 AND ?2  ",nativeQuery=true)
	List<ICPEquipment> findByTimePage( Long startTime, Long endTime,Pageable page);
	
	@Query(value="SELECT * FROM icpequipment WHERE  equip_ctime between ?1 AND ?2 AND equipState=?3 ",nativeQuery=true)
	List<ICPEquipment> findByTimePageState( Long startTime, Long endTime,int state,Pageable page);

	
	
}
