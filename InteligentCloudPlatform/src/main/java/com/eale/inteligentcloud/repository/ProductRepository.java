package com.eale.inteligentcloud.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.eale.inteligentcloud.domain.ICPProduct;

/**
 * repository是mapper的升级版,<Product--是我们要操作的实体类,Long--实体类的主键的数据类型
 * @author Administrator
 *
 */
public interface ProductRepository extends JpaRepository<ICPProduct, Long> {
	
	List<ICPProduct> findByProName(String proName);
	
	
	@Query(value="SELECT * FROM icpproduct WHERE  pro_ctime between ?1 AND ?2  ",nativeQuery=true)
	List<ICPProduct> findByPageAndTime(Long startTime,Long endTime,Pageable page);
	
	
	
}
