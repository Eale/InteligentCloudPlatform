package com.eale.inteligentcloud.exception;

import java.util.List;

import org.springframework.validation.ObjectError;

public class ParamValidatedException extends RuntimeException {

	private List<ObjectError> allErrors;

	public ParamValidatedException(List<ObjectError> allErrors) {
		this.allErrors=allErrors;
	}

	public List<ObjectError> getAllErrors() {
		return allErrors;
	}

	public void setAllErrors(List<ObjectError> allErrors) {
		this.allErrors = allErrors;
	}

}
