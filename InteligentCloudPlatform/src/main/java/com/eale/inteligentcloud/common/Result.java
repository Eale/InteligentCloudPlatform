package com.eale.inteligentcloud.common;

/**
 * http请求返回的最外层对象
 * Created by adminstrator
 * 2017-01-21 13:34
 */
public class Result {

    /** 错误码. */
    private Integer code;
    
    /** 具体的内容. */
    private Object data;

    /** 提示信息. */
    private String msg;
	
	public Result(Integer code, Object data, String msg) {
		super();
		this.code = code;
		this.data = data;
		this.msg = msg;
	}

	public Result() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Result(int i) {
		this.code=i;
	}

	public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
