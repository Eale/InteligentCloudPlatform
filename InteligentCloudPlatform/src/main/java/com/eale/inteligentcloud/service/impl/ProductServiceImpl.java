package com.eale.inteligentcloud.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.domain.ICPProduct;
import com.eale.inteligentcloud.repository.ProductRepository;
import com.eale.inteligentcloud.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductRepository productRepository;
	
	/**
	 * 添加产品
	 */
	@Override
	public void addProduct(ICPProduct product) {
		productRepository.save(product);
		
	}
	
	/**
	 * 将产品状态改为-1
	 */
	@Override
	public void deleteProduct(Long proId) {
		
		Optional<ICPProduct>  opt=productRepository.findById(proId);
		opt.get().setProState(-1);
		productRepository.save(opt.get());
		
	}
	
	/**
	 * 批量删除 产品状态改为-1
	 */
	@Override
	public void deleteProducts(Long[] proFids) {
		for (long i = 0; i < proFids.length; i++) {
			Optional<ICPProduct>  opt =productRepository.findById(proFids[(int) i]);
			if(opt.isPresent()) {
				opt.get().setProState(-1);
				productRepository.save(opt.get());
			}
			
		}
		
	}

	@Override
	public void updateProduct(ICPProduct product) {
		Optional<ICPProduct>  opt=productRepository.findById(product.getProFid());
		opt.get().setProDescribe(product.getProDescribe());
		opt.get().setProImgurl(product.getProImgurl());
		opt.get().setProName(product.getProName());
		opt.get().setProState(product.getProState());
		opt.get().setProUptime(new Date().getTime());
		opt.get().setProUpuser(product.getProUpuser());
		productRepository.save(opt.get());
		
	}
	/**
	 * 按条件 分页查询产品
	 */
	@Override
	public List<ICPProduct> findByConditionsPage(Pageable page, ICPProduct product) {
		Example<ICPProduct> example= Example.of(product);
		Page<ICPProduct> list=productRepository.findAll(example, page);
		return list.getContent();
	}

	
	/**
	 * 修改产品状态为2 
	 */
	@Override
	public void updateState(Long proFid) {
		Optional<ICPProduct>  opt=productRepository.findById(proFid);
		opt.get().setProState(2);
		productRepository.save(opt.get());
	}

	@Override
	public List<ICPProduct> findAllPage(Pageable page) {
		Page<ICPProduct> list = productRepository.findAll(page);
		
		return list.getContent();
	}

	@Override
	public Optional<ICPProduct> findById(Long proFid) {
		
		return productRepository.findById(proFid);
	}

	@Override
	public List<ICPProduct> findConditionsPage(Long beginTime, Long endTime, Pageable page) {
		
		List<ICPProduct> list=productRepository.findByPageAndTime(beginTime, endTime, page);
		return list;
	}
	
	@Override
	public long findAllNum() {
		
		return productRepository.count();
	}

	@Override
	public List<ICPProduct> findAllState(ICPProduct product) {
		Example<ICPProduct> example=Example.of(product);
		
		List<ICPProduct> list = productRepository.findAll(example);
		return list;
	}

	@Override
	public Optional<ICPProduct> findByproName(String proName) {
		List<ICPProduct> list = productRepository.findByProName(proName);
		return Optional.of(list.get(0));
	}

	


}
