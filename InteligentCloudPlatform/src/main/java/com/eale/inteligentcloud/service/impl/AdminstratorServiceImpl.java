 package com.eale.inteligentcloud.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.domain.ICPAdminstrator;
import com.eale.inteligentcloud.repository.AdminstratorRepository;
import com.eale.inteligentcloud.service.AdminstratorService;

@Transactional
@Service
public class AdminstratorServiceImpl implements AdminstratorService {
	
	@Autowired
	 AdminstratorRepository adminstratorrepository;
	
	@Override
	public void addAdmin(ICPAdminstrator adminstrator) {
		
		adminstratorrepository.save(adminstrator);
		
	}

	@Override
	public void deleteAdmin(Long adId) {
		Optional<ICPAdminstrator> admins=adminstratorrepository.findById(adId);
		admins.get().setAdDelete(-1);
		
	}
	
	@Override
	public void deleteAdmins(Long[] adids) {
		for (int i = 0; i < adids.length; i++) {
			Optional<ICPAdminstrator> opt=adminstratorrepository.findById(adids[i]);
			opt.get().setAdDelete(-1);
			adminstratorrepository.save(opt.get());
		}
		
	}

	@Override
	public Object updateAdmin(ICPAdminstrator adminstrator) {
		Optional<ICPAdminstrator> opt=adminstratorrepository.findById(adminstrator.getAdId());
		
		if(opt.isPresent()) {
			ICPAdminstrator admin=opt.get();
			if(null!=adminstrator.getAdCtime()) {
				admin.setAdCtime(adminstrator.getAdCtime());
			}
			if(null!=adminstrator.getAdCuser()) {
				admin.setAdCuser(adminstrator.getAdCuser());
			}
			if(null!=adminstrator.getAdDelete()) {
				admin.setAdDelete(adminstrator.getAdDelete());
			}
			if(null!=adminstrator.getAdGrade()) {
				admin.setAdGrade(adminstrator.getAdGrade());
			}
			if(null!=adminstrator.getAdfactory()) {
				admin.setAdfactory(adminstrator.getAdfactory());
			}
			if(null!=adminstrator.getAdNumber()) {
				admin.setAdNumber(adminstrator.getAdNumber());
			}
			if(null!=adminstrator.getAdPassword()) {
				admin.setAdPassword(adminstrator.getAdPassword());
			}
			if(null!=adminstrator.getAdUptime()) {
				admin.setAdUptime(adminstrator.getAdUptime());
			}
			if(null!=adminstrator.getAdUpuser()) {
				admin.setAdUpuser(adminstrator.getAdCuser());
			}
			
			return adminstratorrepository.save(admin);
		}
		
		return new Result(-100);
	}

	@Override
	public List<ICPAdminstrator> findAll() {
		List<ICPAdminstrator> list=adminstratorrepository.findAll();
		return list;
	}

	@Override
	public Optional<ICPAdminstrator> login(ICPAdminstrator admin) {
		Example<ICPAdminstrator> example=Example.of(admin);
		Optional<ICPAdminstrator> findOne = adminstratorrepository.findOne(example);
		return findOne;
	}

	

	

	@Override
	public List<ICPAdminstrator> findByPage(Pageable page, ICPAdminstrator adminstrator) {
		Example<ICPAdminstrator> example= Example.of(adminstrator);
		 Page<ICPAdminstrator> list=adminstratorrepository.findAll(example, page);
		return list.getContent();
		
	}

	@Override
	public Optional<ICPAdminstrator> findById(Long adId) {
		Optional<ICPAdminstrator> opt = adminstratorrepository.findById(adId);
		return opt;
	}

	@Override
	public List<ICPAdminstrator> findByPageTime(Pageable pageable, Long startTime, Long endTime) {
		 List<ICPAdminstrator> list = adminstratorrepository.findByPageTime( startTime, endTime,pageable);
		return list;
	}

	@Override
	public List<ICPAdminstrator> findByPageTimeState(Pageable pageable, Long startTime, Long endTime, int state) {
		List<ICPAdminstrator> list = adminstratorrepository.findByPageTimeState(startTime, endTime, state, pageable);
		return list;
	}

	

}
