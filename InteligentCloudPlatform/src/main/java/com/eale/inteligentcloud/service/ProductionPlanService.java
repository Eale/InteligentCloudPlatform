package com.eale.inteligentcloud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPOrder;
import com.eale.inteligentcloud.domain.ICPProductionPlan;

/**
 * 生产计划--service
 * @author Administrator
 *
 */
@Transactional
public interface ProductionPlanService {
	/**
	 * 添加生产计划
	 */
	void addProductionPlan(ICPProductionPlan planductionPlan);
	
	/**
	 * 删除生产计划
	 * @param planId
	 */
	void deleteProductionPlan(Long planId);
	
	/**
	 * 批量删除
	 * @param planIds
	 */
	void deleteProductionPlans(Long[] planIds);

    /**
     * 启动生产计划
     * @param planId
     */
	void launchProductionPlans(Long planId);

	
	/**
	 * 修改生产计划
	 * @param planductionPlan
	 */
	void updateProductionPlan(ICPProductionPlan planductionPlan);
	
	/**
	 * 按条件分页查询 
	 * @param page
	 * @param planductionPlan
	 * @return
	 */
	List<ICPProductionPlan> findByConditionsPage(Pageable page, ICPProductionPlan planductionPlan);

    /**
     * 分页查询 全部
     * @param page
     * @return
     */
    List<ICPProductionPlan> findAllPage(Pageable page);

    /**
     * 按fid分页查询
     * @param planFid
     * @return
     */
    Optional<ICPProductionPlan> findById(Long planFid);

    /**
     * 按生产计划创建时间范围查询
     * @param beginTime
     * @param endTime
     * @param page
     * @return
     */
    List<ICPProductionPlan> findConditionsPage(Long beginTime,Long endTime,Pageable page);

    /**
     * 按订单查询
     * @param order
     * @param page
     * @return
     */
    List<ICPProductionPlan> findOrderPage(ICPOrder order, Pageable page);

}
