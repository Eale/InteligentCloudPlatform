package com.eale.inteligentcloud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPEquipment;
/**
 * 设备管理 -service
 * @author Administrator
 *
 */
@Transactional
public interface EquipmentService {
	/**
	 * 新增设备
	 * @param equipment
	 */
	void addEquipment(ICPEquipment equipment);
	
	/**
	 * 按设备状态找出对应状态的设备数量
	 * @param equipment
	 * @return
	 */
	int findEquipType(ICPEquipment equipment);
	
	/**
	 * 按设备状态 计算 状态比率
	 * @param equipment
	 * @return
	 */
	Double figureEquipTypeRate(ICPEquipment equipment);
	
	
	/**
	 * 批量
	 * 将设备状态该为失效(故障)
	 * @param equipment
	 */
	void DeleteEquipment(Long[] equipIds);
	
	/**
	 * 更改设备详情
	 */
	Optional<ICPEquipment> UpdateEquipment(ICPEquipment equipment);
	
	/**
	 * 按条件分页查询设备
	 * @param equipment
	 * @return
	 */
	List<ICPEquipment> findByExample(Pageable page,ICPEquipment equipment);
	
	/**
	 * 按时间分页查询设备
	 * @param page
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	List<ICPEquipment> findByTimePage(Pageable page,Long startTime,Long endTime);
	
	/**
	 * 按分页查询全部
	 * @param pageable
	 * @return
	 */
	List<ICPEquipment> findAllPage(Pageable pageable);
	
	/**
	 * 查询总数量
	 * @return
	 */
	int findNUmber();

	Optional<ICPEquipment> findById(long id);
	
	/**
	 * 删除单个
	 * @param equipId
	 */
	void deleteById(long equipId);
	
	/**
	 * 时间状态分页查询
	 * @param pageable
	 * @param startTime
	 * @param endTime
	 * @param state
	 * @return
	 */
	List<ICPEquipment> findByTimePageState(Pageable pageable, long startTime, long endTime,int state);

	/**
	 * 根据产品查找设备
	 * @param equipSequence
	 * @return
	 */
	List<ICPEquipment> findBySequence(String equipSequence);
	
	/**
	 * 根据条件查询设备
	 * @param equipment
	 * @return
	 */
	List<ICPEquipment> findByCondtions(ICPEquipment equipment);
	
}
