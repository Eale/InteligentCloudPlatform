package com.eale.inteligentcloud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPPersonalInformation;

/**
 * 个人信息--service
 * @author Administrator
 *
 */
@Transactional
public interface PersonalInformationService {
	/**
	 * 添加管理员信息
	 */
	void addPersonalInformation(ICPPersonalInformation personalInformation);
	
	/**
	 * 删除管理员信息
	 * @param proFid
	 */
	void deletePersonalInformation(Long perId);
	/**
	 * 批量删除
	 * @param proFids
	 */
	void deletePersonalInformations(Long[] perIds);
	
	/**
	 * 修改管理员信息
	 * @param personalInformation
	 */
	void updatePersonalInformation(ICPPersonalInformation personalInformation);
	
	/**
	 * 根据ID查询
	 * @param proFid
	 * @return
	 */
    Optional<ICPPersonalInformation> findById(Long proFid);
    
	/**
	 * 按条件分页查询 
	 * @param page
	 * @param personalInformation
	 * @return
	 */
	List<ICPPersonalInformation> findByConditionsPage(Pageable page, ICPPersonalInformation personalInformation);
	
	/**
	 * 根据管理员ID查询个人信息
	 * @param adId
	 * @return
	 */
	Optional<ICPPersonalInformation> findByAdminId(Long adId);
}
