package com.eale.inteligentcloud.service;

/**
 * 订单 --service
 */
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPOrder;

@Transactional
public interface OrderService {
	/**
	 * 下单
	 */
	void PlaceOrder(ICPOrder order);
	
	/**
	 * 修改订单
	 */
	void updateOrder(ICPOrder order);

    /**
     * 接单
     * @param oId
     */
	void acceptOrder(Long oId);

	/**
     * 拒单
     * @param oId
	 * @param oRemark 
     */
	void rejectOrder(Long oId, String oRemark);
	
	/**
	 * 删除订单  修改状态 失效-1
	 */
	void deleteOrder(Long oId);
	
	/**
	 * 批量删除
	 */
	void deleteOrders(Long[] oIds);
	
	/**
	 * 按订单id查询 ;
	 */
	Optional<ICPOrder> findById(Long oId);
	
	/**
	 * 按条件分页查询
	 */
	List<ICPOrder> findByConditionsPage(ICPOrder order, Pageable page);

    /**
     * 按订单创建时间范围查询
     * @param beginTime
     * @param endTime
     * @param page
     * @return
     */
    List<ICPOrder> findConditionsPage(Long beginTime, Long endTime, Pageable page);

    /**
     * 按订单来源分页查询
     */
    List<ICPOrder> findBySoursePage(Integer oSourse, Pageable page);

    /**
     * 转生产计划
     */
    void transformProductionPlan(Long oId);

    /**
	 * 按条件查询订单数量
	 * @return
	 */
	int findByConditionsNum(ICPOrder order);

    /**
     * 查询未删除的订单数量
     * @param oState
     * @return
     */
    int findAllNum(Integer oState);

    /**
     * 查询时间范围内未删除的订单数量
     * @param oState
     * @param beginTime
     * @param endTime
     * @return
     */
    int findAllNum(Integer oState,Long beginTime,Long endTime);
    /**
     * 按订单状态统计订单数量
     * @param oState
     * @return
     */
	int findByStateNum(Integer oState);
	
	/**
	 * 订单产品的完成数量
	 * @param oId
	 * @param ostate
	 * @param proFid
	 * @return
	 */
	int FigureByfinshNum(Integer oId,Integer ostate,Long proFid);
	
	/**
	 * 所有分页查询
	 * @param pageable
	 * @return
	 */
	List<ICPOrder> findAllPage(Pageable pageable);
	
	/**
	 * 根据状态分页查询
	 * @param oState
	 * @param pageable
	 * @return
	 */
	List<ICPOrder> findByStatePage(Integer oState, Pageable pageable);
	
	/**
	 * 已经处理的订单--已拒绝[2]/已接单[3]/生产中[4]/已完成[5]
	 * @param pageable
	 * @return
	 */
	List<ICPOrder> findByStatePageHandled(Pageable pageable,List<Integer> state);
	 
	/**
	 * 转生产计划
	 * @param oId
	 * @param state 
	 * @param planEndtime 
	 * @param planStarttime 
	 * @param planStarttime 
	 * @param planEndtime 
	 */
	void TransforOrderToPlan(Long oId, Integer state, Long planStarttime, Long planEndtime);
	
	/**
	 * 根据条件查询订单
	 * @param planOrder
	 * @return
	 */
	List<ICPOrder> findByExample(ICPOrder planOrder);
	
}
