package com.eale.inteligentcloud.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.domain.ICPOrder;
import com.eale.inteligentcloud.domain.ICPProductionPlan;
import com.eale.inteligentcloud.repository.EquipmentRepository;
import com.eale.inteligentcloud.repository.OrderRepository;
import com.eale.inteligentcloud.repository.ProductionPlanRepository;
import com.eale.inteligentcloud.service.OrderService;
@Transactional
@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	EquipmentRepository equipmentrepository;
	
	@Autowired
	ProductionPlanRepository productionplanrepository;

	@Override
	public void PlaceOrder(ICPOrder order) {
		orderRepository.save(order);

	}

	@Override
	public void updateOrder(ICPOrder order) {
		Optional<ICPOrder> opt=orderRepository.findById(order.getoId());
		opt.get().setoDeadline(order.getoDeadline());
		opt.get().setoFinishNum(order.getoFinishNum());
		opt.get().setoNumber(order.getoNumber());
		opt.get().setoPlacetime(order.getoPlacetime());
		opt.get().setoProduct(order.getoProduct());
		opt.get().setoQuantity(order.getoQuantity());
		opt.get().setoRemark(order.getoRemark());
		opt.get().setoSourse(order.getoSourse());
		opt.get().setoState(order.getoState());
		opt.get().setoUptime(new Date().getTime());
		opt.get().setoUpuser(order.getoUpuser());
		orderRepository.save(opt.get());

	}

	@Override
	public void acceptOrder(Long oId) {
		Optional<ICPOrder> opt=orderRepository.findById(oId);
		if(opt.isPresent()) {
			opt.get().setoState(3);
			opt.get().setoUptime(System.currentTimeMillis());
			opt.get().setoUpuser((long) 1);//测试用户
			orderRepository.save(opt.get());
		}
		
		
	}

	@Override
	public void rejectOrder(Long oId,String oRemark) {
		Optional<ICPOrder> opt=orderRepository.findById(oId);
		opt.get().setoState(2);
		opt.get().setoRemark(oRemark);
		opt.get().setoUptime(System.currentTimeMillis());
		opt.get().setoUpuser((long) 1);//测试用户
		orderRepository.save(opt.get());

	}

	@Override
	public void deleteOrder(Long oId) {
		Optional<ICPOrder> opt=orderRepository.findById(oId);
		if(opt.isPresent()) {
			opt.get().setoState(-1);
			opt.get().setoUptime(System.currentTimeMillis());
			opt.get().setoUpuser((long) 1);//测试用户
			orderRepository.save(opt.get());
		}
	}

	@Override
	public void deleteOrders(Long[] oIds) {
		for (int i = 0; i < oIds.length; i++) {
			Optional<ICPOrder> opt=orderRepository.findById(oIds[i]);
			if(opt.isPresent()) {
				opt.get().setoState(-1);
				opt.get().setoUptime(System.currentTimeMillis());
				opt.get().setoUpuser((long) 1);//测试用户
				orderRepository.save(opt.get());
			}
		}
	}

	@Override
	public Optional<ICPOrder> findById(Long oId) {
		Optional<ICPOrder> opt=orderRepository.findById(oId);
		
		return opt;
	}

	@Override
	public List<ICPOrder> findByConditionsPage(ICPOrder order, Pageable page) {
		Example<ICPOrder> example = Example.of(order);
		Page<ICPOrder> list = orderRepository.findAll(example, page);
		return list.getContent();
	}

	@Override
	public List<ICPOrder> findConditionsPage(Long beginTime, Long endTime, Pageable page) {
		List<ICPOrder> list = orderRepository.findByoState(beginTime, endTime, page);
		return list;
	}

	@Override
	public List<ICPOrder> findBySoursePage(Integer oSourse, Pageable page) {
		ICPOrder order  = new ICPOrder();
		order.setoSourse(oSourse);
		Example<ICPOrder> example = Example.of(order);
		Page<ICPOrder> list = orderRepository.findAll(example, page);
		return list.getContent();
	}

	@Override
	public void transformProductionPlan(Long oId) {
		// TODO Auto-generated method stub

	}

	@Override
	public int findByConditionsNum(ICPOrder order) {
		Example<ICPOrder> example = Example.of(order);
		long mun = orderRepository.count(example);
		return (int) mun;
//		return 0;
	}

	@Override
	public int findAllNum(Integer oState) {
		ICPOrder order  = new ICPOrder();
		order.setoState(oState);
		Example<ICPOrder> example = Example.of(order);
		long mun = orderRepository.count(example);
		long count = orderRepository.count();
		long number=count-mun;
		return (int) number;
	}

	@Override
	public int findAllNum(Integer oState, Long beginTime, Long endTime) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int findByStateNum(Integer oState) {
		ICPOrder order  = new ICPOrder();
		order.setoState(oState);
		Example<ICPOrder> example = Example.of(order);
		long mun = orderRepository.count(example);
		return (int) mun;
	}

	@Override
	public int FigureByfinshNum(Integer oId, Integer ostate, Long proFid) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<ICPOrder> findAllPage(Pageable pageable) {
		Page<ICPOrder> list = orderRepository.findAll(pageable);
		return list.getContent();
	}

	@Override
	public List<ICPOrder> findByStatePage(Integer oState, Pageable pageable) {
		ICPOrder order = new ICPOrder();
		order.setoState(oState);
		Example<ICPOrder> example = Example.of(order);
		
		Page<ICPOrder> list = orderRepository.findAll(example, pageable);
		return list.getContent();
	}

	@Override
	public List<ICPOrder> findByStatePageHandled(Pageable pageable,List<Integer> state) {
		List<ICPOrder> list = orderRepository.findByoStateNotIn(state,pageable);
		return list;
	}

	@Override
	public void TransforOrderToPlan(Long oId, Integer state,Long planStarttime, Long planEndtime) {
		Optional<ICPOrder> opt = orderRepository.findById(oId);
		if(opt.isPresent()) {
			opt.get().setoState(state);
			
			ICPProductionPlan plan=new ICPProductionPlan();
			plan.setPlanCtime(new Date().getTime());
			plan.setPlanCuser((long)1);//测试用户
			plan.setPlanStarttime(planStarttime);
			plan.setPlanEndtime(planEndtime);
			plan.setPlanState(1);
			plan.setPlanOrder(opt.get());
			plan.setPlanTime(new Date().getTime());
			plan.setPlanNumber(new Date().getTime());
			productionplanrepository.save(plan);
			
			orderRepository.save(opt.get());
			
		}
		
	}

	@Override
	public List<ICPOrder> findByExample(ICPOrder planOrder) {
		Example<ICPOrder> example=Example.of(planOrder);
		List<ICPOrder> list = orderRepository.findAll(example);
		return list;
	}

}
