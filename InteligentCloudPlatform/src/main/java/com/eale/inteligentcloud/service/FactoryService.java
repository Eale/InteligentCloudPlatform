package com.eale.inteligentcloud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPFactory;

/**
 * 工厂service
 * @author Administrator
 *
 */
@Transactional
public interface FactoryService {
	
	/**
	 * 添加工厂
	 * @return 
	 */
	Optional<ICPFactory> addFactory(ICPFactory factory);
	
	/**
	 * 删除工厂
	 * @param factory
	 */
	void deleteFactory(Long fid);
	
	/**
	 * 批量删除
	 * @param factorys
	 */
	void deleteFactorys(Long[] fIds);
	
	/**
	 * 修改工厂
	 * @param factory
	 */
	void updateFactory(ICPFactory factory);
	
	/**
	 * 按条件分页查询 
	 * @param page
	 * @param factory
	 * @return
	 */
	List<ICPFactory> findByConditionsPage(Pageable page,ICPFactory factory);
	
	/**
	 * 根据ID查询工厂
	 * @param getfId
	 * @return
	 */
	Optional<ICPFactory> findById(Long getfId);
	
	/**
	 * 时间分页查询
	 * @param pageable
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	List<ICPFactory> findByPageTime(Pageable pageable, long startTime, long endTime);
	
	/**
	 * 根据名字查询工厂
	 */
	List<ICPFactory> findByFactoryName(String name);
	
	/**
	 * 根据状态时间分页查询
	 * @param pageable
	 * @param startTime
	 * @param endTime
	 * @param state
	 * @return
	 */
	List<ICPFactory> findByPageTimeByState(Pageable pageable, long startTime, long endTime, int state);
	
	/**
	 * 全部分页查询
	 * @param pageable
	 * @return
	 */
	List<ICPFactory> findAll(Pageable pageable);
	
	/**
	 * 全部按状态查询
	 * @param factory
	 * @return
	 */
	List<ICPFactory> findAllByState(ICPFactory factory);
}
