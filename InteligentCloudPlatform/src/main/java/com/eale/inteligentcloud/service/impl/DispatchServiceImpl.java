
package com.eale.inteligentcloud.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.domain.ICPDispatch;
import com.eale.inteligentcloud.domain.ICPFollow;
import com.eale.inteligentcloud.repository.DispatchRepository;
import com.eale.inteligentcloud.repository.FollowRepository;
import com.eale.inteligentcloud.service.DispatchService;

@Transactional
@Service
public class DispatchServiceImpl implements DispatchService {

	@Autowired
	DispatchRepository dispatchRepository;
	
	@Autowired
	FollowRepository followRepository;
	
	
	@Override
	public void adddispatch(ICPDispatch dispatch) {
		dispatchRepository.save(dispatch);

	}

	@Override
	public void deletedispatch(Long dispFid) {
		Optional<ICPDispatch> opt=dispatchRepository.findById(dispFid);
		opt.get().setDispState(-1);
		dispatchRepository.save(opt.get());

	}

	@Override
	public void deletedispatches(Long[] dispFids) {
		for (int i = 0; i < dispFids.length; i++) {
			Optional<ICPDispatch> opt=dispatchRepository.findById(dispFids[i]);
			opt.get().setDispState(-1);
			dispatchRepository.save(opt.get());
		}
	}

	@Override
	public void launch(ICPDispatch dispatch) {
		
		Optional<ICPDispatch> opt=dispatchRepository.findById(dispatch.getDispFid());
		opt.get().setDisequipment(dispatch.getDisequipment());
		opt.get().setDisProductionPlan(dispatch.getDisProductionPlan());
		opt.get().setDispBegintiem(new Date().getTime());
		opt.get().setDispUptime(new Date().getTime());
//		opt.get().setDispUpuser(dispUpuser);
		opt.get().setDispState(2);
		
		List<ICPFollow> list=new ArrayList<>();
		ICPFollow follow=new ICPFollow();
		follow.setFollowCtime(new Date().getTime());
		follow.setFollowCuser((long)1);//测试用户
		follow.setFollowDispatch(opt.get());
		follow.setFollowOrder(opt.get().getDisProductionPlan().getPlanOrder());
		
		followRepository.save(follow);
		list.add(follow);
		opt.get().setFollows(list);
		
		dispatchRepository.save(opt.get());
	}

	@Override
	public void updatedispatch(ICPDispatch dispatch) {
		Optional<ICPDispatch> opt=dispatchRepository.findById(dispatch.getDispFid());
		if(opt.isPresent()) {
			if(null!=dispatch.getDispBegintiem()) {
				opt.get().setDispBegintiem(dispatch.getDispBegintiem());
			}
			if(null!=dispatch.getDispCtime()) {
				opt.get().setDispCtime(dispatch.getDispCtime());
			}
			if(null!=dispatch.getDispCuser()) {
				opt.get().setDispCuser(dispatch.getDispCuser());
			}
			if(null!=dispatch.getDispEndtime()) {
				opt.get().setDispEndtime(dispatch.getDispEndtime());
			}
			if(null!=dispatch.getDisProductionPlan()) {
				opt.get().setDisProductionPlan(dispatch.getDisProductionPlan());
			}
			if(null!=dispatch.getDispState()) {
				opt.get().setDispState(dispatch.getDispState());
			}
			if(null!=dispatch.getDispUptime()) {
				opt.get().setDispUptime(dispatch.getDispUptime());
			}
			if(null!=dispatch.getDispUpuser()) {
				opt.get().setDispUpuser(dispatch.getDispUpuser());
			}
			if(null!=dispatch.getFollows()) {
				opt.get().setFollows(dispatch.getFollows());
			}
			dispatchRepository.save(opt.get());
		}
		
		
	}

	@Override
	public Optional<ICPDispatch> findById(Long dispFid) {
		Optional<ICPDispatch> opt = dispatchRepository.findById(dispFid);
		return opt;
	}

	@Override
	public List<ICPDispatch> findByPage(Pageable page) {
		Page<ICPDispatch> list=dispatchRepository.findAll(page);
		
		return list.getContent();
	}

	@Override
	public List<ICPDispatch> findByCondetions(Pageable page, ICPDispatch dispatch) {
		Example<ICPDispatch> example= Example.of(dispatch);
		 Page<ICPDispatch> list=dispatchRepository.findAll(example, page);
		return list.getContent();
		
	}

	@Override
	public List<ICPDispatch> findByTime(Pageable page, Long startTime, Long endTime) {
		 List<ICPDispatch> list = dispatchRepository.findByTime( startTime, endTime,page);
		return list;
	}

	@Override
	public int findcount() {
		return (int) dispatchRepository.count();
	}

	@Override
	public List<ICPDispatch> findByStatePage(Pageable pageable, int dispstate) {
		
		
		List<ICPDispatch> list = dispatchRepository.findByStatePage(dispstate, pageable);
		return list;
	}

	

}
