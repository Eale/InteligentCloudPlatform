package com.eale.inteligentcloud.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.domain.ICPEquipment;
import com.eale.inteligentcloud.repository.EquipmentRepository;
import com.eale.inteligentcloud.service.EquipmentService;

@Transactional
@Service
public class EquipmentServiceImpl implements EquipmentService {
	
	@Autowired 
	EquipmentRepository equipmentRepository;

	@Override
	public void addEquipment(ICPEquipment equipment) {
		equipmentRepository.save(equipment);

		
	}
	/**
	 * 按照设备的状态 查询数量
	 */
	@Override
	public int findEquipType(ICPEquipment equipment) {
		Example<ICPEquipment> example = Example.of(equipment);
		long mun = equipmentRepository.count(example);
		return (int) mun;
	}

	/**
	 * 按照设备状态  计算比率
	 */
	@Override
	public Double figureEquipTypeRate(ICPEquipment equipment) {
		Example<ICPEquipment> example = Example.of(equipment);
		long mun = equipmentRepository.count(example);
		long mun2 = equipmentRepository.count();
		Double rate = (double) (mun/mun2);
		return rate;
	}

	@Override
	public void DeleteEquipment(Long[] equipIds) {
		for (int i = 0; i < equipIds.length; i++) {
			Optional<ICPEquipment> opt=equipmentRepository.findById(equipIds[i]);
			opt.get().setEquipState(-1);
			equipmentRepository.save(opt.get());
		}
		
	}

	@Override
	public Optional<ICPEquipment> UpdateEquipment(ICPEquipment equipment) {
		Optional<ICPEquipment> opt=equipmentRepository.findById(equipment.getEquipId());
		
		if(opt.isPresent()) {
			if(null!=equipment.getEquipCapacity()) {
				opt.get().setEquipCapacity(equipment.getEquipCapacity());
			}
			if(null!=equipment.getEquipCtime()) {
				opt.get().setEquipCtime(equipment.getEquipCtime());
			}
			if(null!=equipment.getEquipCuser()) {
				opt.get().setEquipCuser(equipment.getEquipCuser());
			}
			if(null!=equipment.getEquipfactory()) {
				opt.get().setEquipfactory(equipment.getEquipfactory());
			}
			if(null!=equipment.getEquipImgur()) {
				opt.get().setEquipImgur(equipment.getEquipImgur());
			}
			if(null!=equipment.getEquipName()) {
				opt.get().setEquipName(equipment.getEquipName());
			}
			if(null!=equipment.getEquipProduct()) {
				opt.get().setEquipProduct(equipment.getEquipProduct());
			}
			if(null!=equipment.getEquipSequence()) {
				opt.get().setEquipSequence(equipment.getEquipSequence());
			}
			if(null!=equipment.getEquipState()) {
				opt.get().setEquipState(equipment.getEquipState());
			}
			if(null!=equipment.getEquipTime()) {
				opt.get().setEquipTime(equipment.getEquipTime());
			}
			if(null!=equipment.getEquipUptime()) {
				opt.get().setEquipUptime(equipment.getEquipUptime());
			}
			if(null!=equipment.getEquipUpuser()) {
				opt.get().setEquipUpuser(equipment.getEquipUpuser());
			}
			equipmentRepository.save(opt.get());
			return opt;
		}	
		
		return null;
	}

	@Override
	public List<ICPEquipment> findByExample(Pageable page, ICPEquipment equipment) {
		Example<ICPEquipment> example = Example.of(equipment);
		Page<ICPEquipment> list=equipmentRepository.findAll(example, page);
		return list.getContent();
	}

	@Override
	public List<ICPEquipment> findByTimePage(Pageable page, Long startTime, Long endTime) {
		List<ICPEquipment> list = equipmentRepository.findByTimePage(startTime, endTime,page);
		return list;
	}

	@Override
	public List<ICPEquipment> findAllPage(Pageable page) {
		Page<ICPEquipment> list = equipmentRepository.findAll(page);
		return list.getContent();
	}

	@Override
	public int findNUmber() {
		long mun = equipmentRepository.count();
		return (int) mun;
	}

	@Override
	public Optional<ICPEquipment> findById(long id) {
		Optional<ICPEquipment> opt = equipmentRepository.findById(id);
		return opt;
	}
	@Override
	public void deleteById(long equipId) {
		Optional<ICPEquipment> opt=equipmentRepository.findById(equipId);
		if(opt.isPresent()) {
			opt.get().setEquipState(-1);
			equipmentRepository.save(opt.get());
		}
		
		
		
	}
	@Override
	public List<ICPEquipment> findByTimePageState(Pageable pageable, long startTime, long endTime, int state) {
		List<ICPEquipment> list = equipmentRepository.findByTimePageState(startTime, endTime, state, pageable);
		return list;
	}
	
	@Override
	public List<ICPEquipment> findBySequence(String equipSequence) {
		
		List<ICPEquipment> list = equipmentRepository.findByequipSequence(equipSequence);
		return list;
	}
	@Override
	public List<ICPEquipment> findByCondtions(ICPEquipment equipment) {
		Example<ICPEquipment> example=Example.of(equipment);
		List<ICPEquipment> list = equipmentRepository.findAll(example);
		return list;
	}

}
