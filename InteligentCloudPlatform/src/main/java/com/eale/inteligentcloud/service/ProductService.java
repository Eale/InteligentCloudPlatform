package com.eale.inteligentcloud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPProduct;

/**
 * 产品管理 --service
 * @author Administrator
 *
 */
public interface ProductService {
	
	/**
	 * 添加产品
	 */
	void addProduct(ICPProduct product);
	
	/**
	 * 删除产品
	 * @param proFid
	 */
	void deleteProduct(Long proFid);
	
	/**
	 * 批量删除
	 * @param proFids
	 */
	void deleteProducts(Long[] proFids);
	
	/**
	 * 修改产品
	 * @param product
	 */
	void updateProduct(ICPProduct product);

    /**
     * 修改产品状态(未测)
     * @param proFid
     */
    void updateState(Long proFid);
	/**
	 * 按条件分页查询 
	 * @param page
	 * @param product
	 * @return
	 */
	List<ICPProduct> findByConditionsPage(Pageable page, ICPProduct product);

    /**
     * 分页查询 全部
     * @param page
     * @return
     */
    List<ICPProduct> findAllPage(Pageable page);

    /**
     * 按fid查询
     * @param proFid
     * @return
     */
    Optional<ICPProduct> findById(Long proFid);

    /**
     * 按产品创建时间范围查询
     * @param beginTime
     * @param endTime
     * @param page
     * @return
     */
    List<ICPProduct> findConditionsPage(Long beginTime,Long endTime,Pageable page);

    /**
     * 查询记录数量
     * @param product
     * @return
     */
    long findAllNum();
    
    /**
     * 状态查询
     * @param product
     * @return
     */
	List<ICPProduct> findAllState(ICPProduct product);
	
	/**
	 * 根据产品名称查询产品
	 * @param proName
	 * @return
	 */
	Optional<ICPProduct> findByproName(String proName);

	
}
