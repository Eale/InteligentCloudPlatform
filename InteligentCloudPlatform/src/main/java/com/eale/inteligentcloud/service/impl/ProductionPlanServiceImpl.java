package com.eale.inteligentcloud.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eale.inteligentcloud.domain.ICPDispatch;
import com.eale.inteligentcloud.domain.ICPOrder;
import com.eale.inteligentcloud.domain.ICPProductionPlan;
import com.eale.inteligentcloud.repository.DispatchRepository;
import com.eale.inteligentcloud.repository.ProductionPlanRepository;
import com.eale.inteligentcloud.service.ProductionPlanService;

@Transactional
@Service
public class ProductionPlanServiceImpl implements ProductionPlanService {

	@Autowired
	ProductionPlanRepository productionPlanRepository;
	
	@Autowired
	DispatchRepository dispatchrepository;
	
	@Override
	public void addProductionPlan(ICPProductionPlan planductionPlan) {
		productionPlanRepository.save(planductionPlan);	
	}

	@Override
	public void deleteProductionPlan(Long planId) {
		Optional<ICPProductionPlan> opt =productionPlanRepository.findById(planId);
		opt.get().setPlanState(-1);
		productionPlanRepository.save(opt.get());
		
	}

	@Override
	public void deleteProductionPlans(Long[] planIds) {
		for (int i = 0; i < planIds.length; i++) {
			Optional<ICPProductionPlan> opt =productionPlanRepository.findById(planIds[i]);
			opt.get().setPlanState(-1);
			productionPlanRepository.save(opt.get());
		}
		
	}

	//状态修改为 2
	@Override
	public void launchProductionPlans(Long planId) {
		Optional<ICPProductionPlan> opt =productionPlanRepository.findById(planId);
		if(opt.isPresent()) {
			opt.get().setPlanState(2);
			productionPlanRepository.save(opt.get());
					
			ICPDispatch dispatch=new ICPDispatch();
			dispatch.setDisProductionPlan(opt.get());
			dispatch.setDispCtime(new Date().getTime());
			dispatch.setDispCuser((long)1);//测试用户
			dispatch.setDispState(1);
			dispatchrepository.save(dispatch);
		
		}	
		
	}

	@Override
	public void updateProductionPlan(ICPProductionPlan planductionPlan) {
		Optional<ICPProductionPlan> opt = productionPlanRepository.findById(planductionPlan.getPlanId());
		opt.get().setPlanEndtime(planductionPlan.getPlanEndtime());
		opt.get().setPlanOrder(planductionPlan.getPlanOrder());
		opt.get().setPlanStarttime(planductionPlan.getPlanStarttime());
		opt.get().setPlanState(planductionPlan.getPlanState());
		opt.get().setPlanTime(planductionPlan.getPlanTime());
		opt.get().setPlanUptime(new Date().getTime());
		opt.get().setPlanUpuser(planductionPlan.getPlanUpuser());
		productionPlanRepository.save(opt.get());
		
		
	}

	@Override
	public List<ICPProductionPlan> findByConditionsPage(Pageable page, ICPProductionPlan planductionPlan) {
		Example<ICPProductionPlan> example = Example.of(planductionPlan);
		Page<ICPProductionPlan> list = productionPlanRepository.findAll(example, page);
		
		return list.getContent();
	}

	@Override
	public List<ICPProductionPlan> findAllPage(Pageable page) {
		Page<ICPProductionPlan> list = productionPlanRepository.findAll(page);
		
		return list.getContent();
		
	}

	@Override
	public Optional<ICPProductionPlan> findById(Long planFid) {
		Optional<ICPProductionPlan> opt =productionPlanRepository.findById(planFid);
		return opt;
	}

	@Override
	public List<ICPProductionPlan> findConditionsPage(Long beginTime, Long endTime, Pageable page) {
		List<ICPProductionPlan> list=productionPlanRepository.findByPageAndTime(beginTime, endTime, page);
		return list;
	}

	@Override
	public List<ICPProductionPlan> findOrderPage(ICPOrder order, Pageable page) {
		ICPProductionPlan plan=new ICPProductionPlan();
		plan.setPlanOrder(order);
		Example<ICPProductionPlan> example = Example.of(plan);
		Page<ICPProductionPlan> list = productionPlanRepository.findAll(example, page);
		
		return list.getContent();
	}

	

}
