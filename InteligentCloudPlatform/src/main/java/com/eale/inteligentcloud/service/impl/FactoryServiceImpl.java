package com.eale.inteligentcloud.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.domain.ICPFactory;
import com.eale.inteligentcloud.repository.FactoryRepository;
import com.eale.inteligentcloud.service.FactoryService;
@Transactional
@Service
public class FactoryServiceImpl implements FactoryService {

	@Autowired
	FactoryRepository factoryRepository;
	

	@Override
	public void deleteFactorys(Long[] fIds) {
		for (int i = 0; i < fIds.length; i++) {
			Optional<ICPFactory> opt=factoryRepository.findById(fIds[i]);
			opt.get().setfStatu(-1);
			factoryRepository.save(opt.get());
		}
	}

	@Override
	public void updateFactory(ICPFactory factory) {
		Optional<ICPFactory> opt=factoryRepository.findById(factory.getfId());
		if(opt.isPresent()) {
			if(null!=factory.getfCtime()) {
				opt.get().setfCtime(factory.getfCtime());
			}
			if(null!=factory.getfCuser()) {
				opt.get().setfCuser(factory.getfCuser());
			}
			if(null!=factory.getfName()) {
				opt.get().setfName(factory.getfName());
			}
			if(null!=factory.getfStatu()) {
				opt.get().setfStatu(factory.getfStatu());
			}
			if(null!=factory.getfUptime()) {
				opt.get().setfUptime(factory.getfUptime());
			}
			if(null!=factory.getfUpuser()) {
				opt.get().setfUpuser(factory.getfUpuser());
			}
			
			factoryRepository.save(opt.get());
			
		}		
		
		
	}

	@Override
	public List<ICPFactory> findByConditionsPage(Pageable page, ICPFactory factory) {
		Example<ICPFactory> example = Example.of(factory);
		Page<ICPFactory> list = factoryRepository.findAll(example,page);
		return list.getContent();
	}

	@Override
	public Optional<ICPFactory> addFactory(ICPFactory factory) {
		factoryRepository.save(factory);
		Optional<ICPFactory> opt = factoryRepository.findById(factory.getfId());
		return opt;
	}

	@Override
	public void deleteFactory(Long fid) {
		Optional<ICPFactory> opt=factoryRepository.findById(fid);
		opt.get().setfStatu(-1);
		factoryRepository.save(opt.get());
		
	}

	@Override
	public Optional<ICPFactory> findById(Long getfId) {
		Optional<ICPFactory> opt = factoryRepository.findById(getfId);
		return opt;
	}

	@Override
	public List<ICPFactory> findByPageTime(Pageable pageable, long startTime, long endTime) {
		List<ICPFactory> list = factoryRepository.findByPageTime(startTime, endTime,pageable);
		return list;
	}

	@Override
	public List<ICPFactory> findByFactoryName(String fname) {
		List<ICPFactory> list = factoryRepository.findByFName(fname);
		return list;
	}

	@Override
	public List<ICPFactory> findByPageTimeByState(Pageable pageable, long startTime, long endTime, int state) {
		List<ICPFactory> list = factoryRepository.findByPageTimeState(startTime, endTime, state, pageable);
		return list;
	}

	@Override
	public List<ICPFactory> findAll(Pageable pageable) {
		Page<ICPFactory> page = factoryRepository.findAll(pageable);
		return page.getContent();
	}

	@Override
	public List<ICPFactory> findAllByState(ICPFactory factory) {
		Example<ICPFactory> example=Example.of(factory);
		List<ICPFactory> list = factoryRepository.findAll(example);
		return list;
	}

	

}
