package com.eale.inteligentcloud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPAdminstrator;

@Transactional
public interface AdminstratorService {
	/**
	 * 新增管理员
	 * @param adminstrator
	 */
	void addAdmin(ICPAdminstrator adminstrator);
	
	/**
	 * 删除
	 * 将管理员状态改为删除态 -1
	 *
	 */
	void deleteAdmin(Long adId);
	
	/**
	 * 批量删除
	 * @param adids
	 */
	void deleteAdmins(Long[] adids);
	
	/**
	 * 修改管理员信息
	 * @param adminstrator
	 * @return
	 */
	Object updateAdmin(ICPAdminstrator adminstrator);
	
	/**
	 * 管理员登录
	 * @param id
	 * @return
	 */
	Optional<ICPAdminstrator> login(ICPAdminstrator admin);
	
	/**
	 * 按条件查询管理员信息
	 * @param adminstrator
	 * @return
	 */
	List<ICPAdminstrator> findByPage(Pageable page,ICPAdminstrator adminstrator);
	
	/**
	 * 查询所有管理员
	 * @return
	 */
	List<ICPAdminstrator> findAll();
	
	/**
	 * 根据ID查询
	 * @param adId
	 * @return
	 */
	Optional<ICPAdminstrator> findById(Long adId);
	
	/**
	 * 根据时间分页查询
	 * @param pageable
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	List<ICPAdminstrator> findByPageTime(Pageable pageable, Long startTime, Long endTime);
	
	/**
	 * 根据时间状态分页查询
	 * @param pageable
	 * @param startTime
	 * @param endTime
	 * @param state
	 * @return
	 */
	List<ICPAdminstrator> findByPageTimeState(Pageable pageable, Long startTime, Long endTime, int state);

}
