package com.eale.inteligentcloud.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.domain.ICPAdminstrator;
import com.eale.inteligentcloud.domain.ICPPersonalInformation;
import com.eale.inteligentcloud.repository.PersonalInformationRepository;
import com.eale.inteligentcloud.service.PersonalInformationService;

@Transactional
@Service
public class PersonalInformationServiceImpl implements PersonalInformationService {

	@Autowired
	PersonalInformationRepository personalInformationRepository;
	
	
	@Override
	public void addPersonalInformation(ICPPersonalInformation personalInformation) {
		personalInformationRepository.save(personalInformation);


	}

	@Override
	public void deletePersonalInformation(Long perId) {
		Optional<ICPPersonalInformation> opt = personalInformationRepository.findById(perId);
		opt.get().setState(-1);
		personalInformationRepository.save(opt.get());

	}

	@Override
	public void deletePersonalInformations(Long[] perIds) {
		for (int i = 0; i < perIds.length; i++) {
			Optional<ICPPersonalInformation> opt = personalInformationRepository.findById(perIds[i]);
			opt.get().setState(-1);
			personalInformationRepository.save(opt.get());
		}
	}

	@Override
	public void updatePersonalInformation(ICPPersonalInformation personalInformation) {
		Optional<ICPPersonalInformation> opt = personalInformationRepository.findById(personalInformation.getPerId());
		 opt.get().setAdmins(personalInformation.getAdmins());
		 opt.get().setPerAnlage(personalInformation.getPerAnlage());
		 opt.get().setPerCity(personalInformation.getPerCity());
		 opt.get().setPerEmail(personalInformation.getPerEmail());
		 opt.get().setPerGender(personalInformation.getPerGender());
		 opt.get().setPerName(personalInformation.getPerName());
		 opt.get().setPerPhone(personalInformation.getPerPhone());
		 opt.get().setPerRemark(personalInformation.getPerRemark());
		 opt.get().setPerUptime(new Date().getTime());
		 opt.get().setPerUpuser(personalInformation.getPerUpuser());
		 opt.get().setState(personalInformation.getState());
		 personalInformationRepository.save(opt.get());
	}


	@Override
	public List<ICPPersonalInformation> findByConditionsPage(Pageable page, ICPPersonalInformation personalInformation) {
		Example<ICPPersonalInformation> example = Example.of(personalInformation);
		Page<ICPPersonalInformation> list = personalInformationRepository.findAll(example, page);
		return list.getContent();
	}

	@Override
	public Optional<ICPPersonalInformation> findById(Long proFid) {
		Optional<ICPPersonalInformation> opt = personalInformationRepository.findById(proFid);
		return opt;
		
	}

	@Override
	public Optional<ICPPersonalInformation> findByAdminId(Long adId) {
		ICPAdminstrator admin=new ICPAdminstrator();
		admin.setAdId(adId);
		Optional<ICPPersonalInformation> opt = personalInformationRepository.findByAdmins(admin);
		return opt;
	}

}
