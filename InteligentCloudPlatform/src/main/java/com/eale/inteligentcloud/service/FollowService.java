package com.eale.inteligentcloud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPFollow;

@Transactional
public interface FollowService {
	/**
	 * 新建 工单跟踪
	 * @param follow
	 */
	void addFollow(ICPFollow follow);
	
	/**
	 * 报工
	 * @param follow
	 */
	void submitRepairOrder(ICPFollow follow);
	
	/**
	 * 结束报工
	 * @param followId
	 */
	void overRepairOrder(Long followId);
	
	/**
	 * 根据报工单号查询 报工单
	 * @param follow
	 * @return
	 */
	Optional<ICPFollow> findById(ICPFollow follow);

	/**
	 * 按条件 分页查询 工单跟踪
	 * @param Follow
	 * @return
	 */
	List<ICPFollow> findByExamle(Pageable page,ICPFollow Follow);
	
	/**
	 * 按订单 分页查询 工单跟踪
	 * @param ICPFollow
	 * @return
	 */
	List<ICPFollow> findByOrder(Pageable page,Long oID);
	
	/**
	 * 分页查询 所有 工单跟踪
	 * @return
	 */
	List<ICPFollow> findAllFollow(Pageable page);
	
	/**
	 * 按时间分页查询报工单
	 * @param page
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	List<ICPFollow> findByTimeFollow(Pageable page,Long startTime,Long endTime);
	
	/**
	 * 修改 工单跟踪
	 * @param follow
	 */
	void updateFollow(ICPFollow follow);
	
	/**
	 * 删除 工单跟踪  状态改为无效
	 * @param follow
	 */
	void deleteFollow(Long followId);
	
	/**
	 * 批量删除 工单跟踪  状态改为无效
	 * @param follow
	 */
	void deleteFollows(Long[] followIds);

	
	/**
	 * 查询所有数量
	 * @return
	 */
	int findNumber();
	
	/**
	 * 条件查询所拥有数量
	 * @param oIds
	 * @return
	 */
	int findbyFollowNumber(Long oIds);
	
	/**
	 * 根据是否已经报工分页查询 
	 * @return
	 */	
	List<ICPFollow> findByStatePage(Pageable pageable, int followState);
	
	/**
	 * 订单里面的产品的完成数量
	 * @param oId
	 * @param proFid
	 * @return
	 */
	int findbyFollowNum(Long oId);
	
	
	
}
