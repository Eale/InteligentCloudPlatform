package com.eale.inteligentcloud.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.eale.inteligentcloud.domain.ICPFollow;
import com.eale.inteligentcloud.domain.ICPOrder;
import com.eale.inteligentcloud.repository.FollowRepository;
import com.eale.inteligentcloud.service.FollowService;
@Transactional
@Service
public class FollowServiceImpl implements FollowService {

	@Autowired
	FollowRepository followRepository;
	
	@Override
	public void addFollow(ICPFollow follow) {
		followRepository.save(follow);
	}

	@Override
	public void submitRepairOrder(ICPFollow follow) {
		Optional<ICPFollow> opt = followRepository.findById(follow.getFollowId());
		if(opt.isPresent()) {
			opt.get().setFollowUptime(new Date().getTime());
			opt.get().setFollowUpuser((long)1);//测试用户
			opt.get().setFollowProcessNum(follow.getFollowProcessNum());
			opt.get().setFollowEndTime(follow.getFollowEndTime());
			opt.get().setFollowQualifyNum(follow.getFollowQualifyNum());
			opt.get().setFollowStratTime(follow.getFollowStratTime());
			opt.get().setFollowState(1);
			followRepository.save(opt.get());
		}
		

	}

	@Override
	public void overRepairOrder(Long followId) {
		// TODO Auto-generated method stub

	}

	@Override
	public Optional<ICPFollow> findById(ICPFollow follow) {
		Optional<ICPFollow> opt=followRepository.findById(follow.getFollowId());
		return opt;
	}

	@Override
	public List<ICPFollow> findByExamle(Pageable page, ICPFollow Follow) {
		Example<ICPFollow> example = Example.of(Follow);
		Page<ICPFollow> list = followRepository.findAll(example,page);
		return list.getContent();
	}

	@Override
	public List<ICPFollow> findByOrder(Pageable page, Long oID) {
		ICPOrder order=new ICPOrder();
		order.setoId(oID);
		ICPFollow follow=new ICPFollow();
		follow.setFollowOrder(order);
		Example<ICPFollow> example = Example.of(follow);
		Page<ICPFollow> list = followRepository.findAll(example,page);
		
		return list.getContent();
	}

	@Override
	public List<ICPFollow> findAllFollow(Pageable page) {
		Page<ICPFollow> list = followRepository.findAll(page);
		return list.getContent();
	}

	@Override
	public List<ICPFollow> findByTimeFollow(Pageable page, Long startTime, Long endTime) {
		
		List<ICPFollow> list = followRepository.findByPageTime(startTime, endTime, page);
		return list;
	}

	@Override
	public void updateFollow(ICPFollow follow) {
		Optional<ICPFollow> opt=followRepository.findById(follow.getFollowId());
		opt.get().setFollowDispatch(follow.getFollowDispatch());
		opt.get().setFollowEndTime(follow.getFollowEndTime());
		opt.get().setFollowProcessNum(follow.getFollowProcessNum());
		opt.get().setFollowQualifyNum(follow.getFollowQualifyNum());
		opt.get().setFollowStratTime(follow.getFollowStratTime());
		opt.get().setFollowType(follow.getFollowType());
		opt.get().setFollowUptime(new Date().getTime());
		opt.get().setFollowUpuser(follow.getFollowUpuser());
		followRepository.save(opt.get());
	}

	@Override
	public void deleteFollow(Long followId) {
		Optional<ICPFollow> opt = followRepository.findById(followId);
		if(opt.isPresent()) {
			opt.get().setFollowState(-1);
		}
	}

	@Override
	public void deleteFollows(Long[] followIds) {
		// TODO Auto-generated method stub

	}

	@Override
	public int findNumber() {
		 long mun=followRepository.count();
		return (int) mun;
	}


	@Override
	public int findbyFollowNumber(Long oIds) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<ICPFollow> findByStatePage(Pageable pageable, int followState) {
		ICPFollow follow=new ICPFollow();
		follow.setFollowState(followState);
		Example<ICPFollow> example = Example.of(follow);
		Page<ICPFollow> list = followRepository.findAll(example,pageable);
		
		return list.getContent();
	}

	@Override
	public int findbyFollowNum(Long oId) {
		ICPOrder order=new ICPOrder();
		order.setoId(oId);
		ICPFollow follow=new ICPFollow();
		follow.setFollowOrder(order);
		follow.setFollowState(1);
		Example<ICPFollow> example=Example.of(follow);
		List<ICPFollow> list = followRepository.findAll(example);
		int qualifyNum=0;
		for (ICPFollow icpFollow : list) {
			qualifyNum += icpFollow.getFollowQualifyNum();
		}
		return qualifyNum;
	}

}
