package com.eale.inteligentcloud.service;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.eale.inteligentcloud.domain.ICPDispatch;

/**
 * 生产调度管理--service
 * @author Administrator
 *
 */
@Transactional
public interface DispatchService {
	
	/**
	 * 添加生产调度/工单
	 */
	void adddispatch(ICPDispatch dispatch);
	
	/**
	 * 删除单个/改为失效状台-1
	 */
	void deletedispatch(Long dispFid);
	
	/**
	 * 批量删除/改为失效状台-1
	 * @param dispatches
	 */
	void deletedispatches(Long[] dispFids);
	
	/**
	 * 启动工单
	 * @param dispatch
	 */
	void launch(ICPDispatch dispatch);
	
	
	/**
	 * 修改生产调度/工单
	 * @param dispatch
	 */
	void updatedispatch(ICPDispatch dispatch);
	
	/**
	 *  根据ID查询
	 * @param disFid 
	 * @return
	 */
	Optional<ICPDispatch> findById(Long disFid);
	
	/**
	 * 全部分页查询
	 * @param page
	 * @return
	 */
	List<ICPDispatch> findByPage(Pageable page);
	
	
	/**
	 * 根据条件分页查询
	 * @param page
	 * @param dispatch
	 */
	List<ICPDispatch> findByCondetions(Pageable page,ICPDispatch dispatch);
	
	/**
	 * 根据时间分页查询
	 * @param page
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	List<ICPDispatch> findByTime(Pageable page,Long startTime,Long endTime);
	
	/**
	 * 查询所有数量
	 * @return
	 */
	int findcount();
	
	/**
	 * 根据状态分页查询
	 * @param pageable
	 * @param followState
	 * @return
	 */
	List<ICPDispatch> findByStatePage(Pageable pageable, int dispState);
	
	
}
