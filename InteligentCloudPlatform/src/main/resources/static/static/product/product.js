$(function () {
    var page = 1
    var total = 5
    var pages=100
    var list

    //加载page页面
    $(".page").load($(".page").attr("data-src"))

    //获取所有产品名称


    //搜索


    //取产品
    bingtab(page, total)

    //没有显示记录数
    $pageSize = $(".pageSize")
    $pageSize.change(function () {
        total = $pageSize.val()
        bingtab(page, total)
    })

    /**
     * 显示一条记录
     */
    $('body').delegate('input[ng-msg="update"]', 'click', function () {
        var $this = $(this);
        var proFid = $this.attr("ng-fid")

        var data = list.find(function (e) {
            return e.proFid == proFid
        })
        $model = $("#updateModal")

        $model.find('input[name="proFid"]').val(data.proFid)
        $model.find('input[name="proName"]').val(data.proName)
        $model.find('input[name="proImgurl"]').val(data.proImgurl)
        $model.find('input[name="proDescribe"]').val(data.proDescribe)
        $model.find('input[name="proState"]').val(data.proState)

        $model.find('button[id="updateProduct"]').attr('ng-fid', data.proFid)

    })
    /**
     * 修改
     */
    $('body').delegate('#updateProduct', 'click', function () {
        $.ajax({
            type: "put",
            url: "/product/product.do",
            data: $(".updateProductForm").serialize(),
            dataType: 'json',
            success: function (obj) {
                bingtab(page, total)
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })
    /**
     * 删除
     */
    $('body').delegate('input[ng-msg="del"]', 'click', function () {
        var $this = $(this);
        var proFid = $(this).attr("ng-fid")
        $.ajax({

            type: "delete",
            url: "/product/product.do",
            data: {
                "proFid": proFid
            },
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    $this.parents("tr").remove()
                }


            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 点击添加按钮
     */
    $("#addBtn").click(function () {
        $.ajax({
            type: "get",
            url: "/factory/factory.do",
            dataType: 'json',
            success: function (datas) {
                console.log(datas)
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })
    /**
     * 增加
     */
    $('body').delegate('#addProduct', 'click', function () {
        $.ajax({
            type: "post",
            url: "/product/product.do",
            data: $(".addProductForm").serialize(),
            dataType: 'json',
            success: function (obj) {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    bingtab(page, total)
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 分页
     */

    // $page = $(".page")
    $('body').delegate('.page li', 'click', function () {
    // $page.find("li").click(function () {
        $this = $(this)
        $previous = $this.find('a[aria-label="Previous"]')
        $next = $this.find('a[aria-label="Next"]')

        //上一页
        function previous() {
            if (page > 1) {
                page--;
                bingtab(page, total)
            } else {
                alert("没有上一页啦!")
            }
            return
        }

        //下一页
        function next() {
            if (page < pages) {
                page++;
                bingtab(page, total)
            } else {
                alert("没有下一页啦!")
            }
            return
        }

        btnpage = $this.find("a").text()
        if (btnpage == "pre") {
            previous()
            $(".thisPage").val(page)
            return
        }
        if (btnpage == "next") {
            next()
            $(".thisPage").val(page)
            return
        }

        $(".thisPage").val(page)
        bingtab(btnpage, total)


    })

    /**
     * 获取
     * @param page
     * @param total
     */
    function bingtab(page, total) {
        $("table[ng-src]").each(function () {
            var $this = $(this);
            var $tbody = $this.children("tbody")
            $tbody.empty()
            $.ajax({
                url: $this.attr("ng-src"),
                type: "get",
                data: {
                    "page": page - 1,
                    "total": total
                },
                success: function (obj) {
                    list = obj.data;
                    var $ths = $this.find('tr[ng-tilte="bingTable"]').find("th");
                    var feilds = [];
                    $ths.each(function () {
                        feilds.push($(this).attr("ng-feild"));
                    })


                    for (var i = 0; i < list.length; i++) {
                        var o = list[i]

                        var $tr = $("<tr ng-fid=" + list[i].proFid + "></tr>")

                        var $check = $('<input type="checkbox" name="checkbox" ng-fid="' + list[i].proFid + '">')
                        var $operation = $('<input class="btn" type="button"  ng-fid="' + list[i].proFid + '" data-toggle="modal" data-target="#updateModal" ng-msg="update" value="修改">' +
                            '<input class="btn" type="button"  ng-fid="' + list[i].proFid + '" ng-msg="del" value="删除">')
                        for (var j = 0; j < feilds.length; j++) {
                            var $td = $("<td name=" + $ths.eq(j).attr("ng-feild") + "></td>")
                            if ($ths.eq(j).attr("ng-feild") == "checkbox") {
                                $td.html($check)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "operation") {
                                $td.html($operation)
                            }
                            $td.text(o[feilds[j]])
                            $tr.append($td)
                        }

                        $tbody.append($tr)
                    }
                },
                dataType: "json"
            })
        })
    }
})