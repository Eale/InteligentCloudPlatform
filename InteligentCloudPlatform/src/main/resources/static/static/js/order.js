$(function () {
    var page = 1
    var total = 5
    var pages = 100
    var list
    var beginTime = null
    var endTime = null

    //获取订单
    getOrder(beginTime,endTime,page, total)

    var  $addOrderForm =$(".addOrderForm")
    var $product="<tr>"+$(".addOrderForm #product").html()+"</tr>"

    $('#addProduct').click(function () {
        $addOrderForm.find("table").append($product)
    })
    $('body').delegate('.addOrderForm #delProduct', 'click', function () {
        $(this).parents("tr").remove()
    })

    //没有显示记录数
    $pageSize = $(".pageSize")
    $pageSize.change(function () {
        total = $pageSize.val()
        if(null!=beginTime&&null!=endTime){
             getOrder(beginTime,endTime,page, total)
        }else {
             getOrder(beginTime,endTime,page, total)
        }

    })

    //刷新
    $("#res").click(function () {
        page=1
        $formsearch.find("input").val("")
        beginTime=null
        endTime=null
        $("table[ng-src]").attr("ng-src","/order/searchOrderStatePage.do");
        getOrder(beginTime,endTime,page, total)
    })

    //搜索
    var $search = $("#search")
    var $formsearch=$(".form-search")

    $search.click(function () {
        page = 1
        $("table[ng-src]").each(function () {
            beginTime=Date.parse($formsearch.find('input[name="begintime"]').val())
            endTime=Date.parse($formsearch.find('input[name="endtime"]').val())
            $("table[ng-src]").attr("ng-src","/order/searchOrderTime.do");
            getOrder(beginTime,endTime,page, total)

        })
    })


    /**
     * 点击添加按钮
     */
    $("#addBtn").click(function () {
        $.ajax({
            type: "get",
            url: "/product/productAllState.do",
            dataType: 'json',
            success: function (obj) {
                var data = obj.data
                var $selectProduct = $("#selectProduct")
                $selectProduct.empty()
                for (var i = 0; i < data.length; i++) {
                    $selectProduct.append("<option>" + data[i].proName + "</option>")
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })
    /**
     * 增加
     */
    $('body').delegate('#addOrder', 'click', function () {
        $.ajax({
            type: "post",
            url: "/order/order.do",
            // data: $(".addOrderForm").serialize(),
            data: {
                "oSourse":$("select[name='oSourse']").val(),
                "oDeadline":Date.parse($("input[name='oDeadline']").val()),
                "oProduct.proName":$("select[name='oProduct.proName']").val(),
                "oQuantity":$("input[name='oQuantity']").val()


            },
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (null != obj.msg) {
                    alert(obj.msg)
                    return
                }
                if (xhr.readyState == 4 && xhr.status == 200) {
                   getOrder(beginTime,endTime,page, total)
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 接单
     */
    $('body').delegate('input[ng-msg="accept"]', 'click', function () {
        var $this = $(this);
        var oId = $(this).parents("tr").attr("ng-fid")
        $.ajax({
            type: "put",
            url: "/order/acceptOrder.do",
            data: {
                "oId": oId
            },
            dataType: 'json',
            success: function (obj, state, xhr) {
            	if(obj.msg="产能不足"){
            		alert(obj.msg)
            	}else{
            		if (xhr.readyState == 4 && xhr.status == 200) {
                        $this.parents("tr").remove()
                    }
            	}
                
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 拒单
     */

    $('body').delegate('input[ng-msg="reject"]', 'click', function () {
        var oId = $(this).parents("tr").attr("ng-fid")
        $("#rejectModal").find('#rejectOrder').attr("ng-fid",oId)

    })
    /**
     * 确定拒单
     */
    $('body').delegate('#rejectOrder', 'click', function () {
        var oId = $(this).attr("ng-fid")
        $.ajax({
            type: "put",
            url: "/order/rejectOrder.do",
            data: {
                "oId": oId,
                "oRemark":$("input[name='oRemark']").val()
            },
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    $(".table").find('tr[ng-fid="'+oId+'"]').remove()
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 查看详情
     */
    $('body').delegate('input[ng-msg="detail"]', 'click', function () {
        var $this = $(this);
        var oId = $this.parents("tr").attr("ng-fid")

        var data = list.find(function (e) {
            return e.oId == oId
        })

        $model = $("#detailModal")


        $model.find('td[name="oNumber"]').text(data.oNumber)
        $model.find('td[name="oSourse"]').text(data.oSourse)
        $model.find('td[name="proName"]').text(data.oProduct.proName)
        $model.find('td[name="oQuantity"]').text(data.oQuantity)
        $model.find('td[name="oDeadline"]').text(new Date(data.oDeadline).toLocaleString())
        $model.find('td[name="oPlacetime"]').text(data.oPlacetime)
        $model.find('td[name="oState"]').text(data.oState)
    })

    /**
     * 删除
     */
    $('body').delegate('input[ng-msg="del"]', 'click', function () {
        var $this = $(this);
        var oId = $(this).parents("tr").attr("ng-fid")
        $.ajax({

            type: "delete",
            url: "/order/order.do",
            data: {
                "oId": oId
            },
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    $this.parents("tr").remove()
                }


            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 全选
     */
    var $checkAll = $("#checkAll")
    $checkAll.change(function () {
        var isChecked = $checkAll.prop("checked")
        $checkAll.parents("table").find("input").prop("checked", isChecked)
    })

    /**
     * 批量删除
     */

    var $dels = $("#dels")
    $dels.click(function () {
        var oIds=[]
        $(".table input[name='checkbox']:checked").each(function () { // 遍历选中的checkbox
            oId = $(this).parents("tr").attr("ng-fid"); // 获取checkbox所在行的顺序
            oIds.push(oId)
        })
        console.log(oIds)
        $.ajax({

            type: "delete",
            url: "/order/orders.do",
            data: {
                "oIds": oIds
            },
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    for (var i = 0; i < oIds.length; i++) {
                        $(".table").find("tr[ng-fid="+oIds[i]+"]").remove()
                    }
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })
    /**
     * 分页
     */

    // $page = $(".page")
    $('body').delegate('.page li', 'click', function () {
        // $page.find("li").click(function () {
        $this = $(this)
        $previous = $this.find('a[aria-label="Previous"]')
        $next = $this.find('a[aria-label="Next"]')

        //上一页
        function previous() {
            if (page > 1) {
                page--;
               getOrder(beginTime,endTime,page, total)
            } else {
                alert("没有上一页啦!")
            }
            return
        }

        //下一页
        function next() {
            if (page < pages) {
                page++;
               getOrder(beginTime,endTime,page, total)
            } else {
                alert("没有下一页啦!")
            }
            return
        }

        btnpage = $this.find("a").text()
        if (btnpage == "pre") {
            previous()
            $(".thisPage").val(page)
            return
        }
        if (btnpage == "next") {
            next()
            $(".thisPage").val(page)
            return
        }

        $(".thisPage").val(page)
       getOrder(beginTime,endTime,page, total)



    })

    /**
     * 获取列表
     * @param page
     * @param total
     */
    function getOrder(beginTime,endTime,page, total) {
        $("table[ng-src]").each(function () {
            var $this = $(this);
            var $tbody = $this.children("tbody")
            $tbody.empty()
            $.ajax({
                url: $this.attr("ng-src"),
                type: "get",
                data: {
                    "beginTime": beginTime,
                    "endTime": endTime,
                    "page": page - 1,
                    "total": total
                },
                success: function (obj) {
                    if (null == obj.data) {
                        alert(obj.msg)
                        return
                    }
                    list = obj.data;
                    var $ths = $this.find('tr[ng-tilte="bingTable"]').find("th");
                    var feilds = [];
                    $ths.each(function () {
                        feilds.push($(this).attr("ng-feild"));
                    })


                    for (var i = 0; i < list.length; i++) {
                        var o = list[i]

                        var $tr = $("<tr ng-fid=" + list[i].oId + "></tr>")

                        var $check = $('<input type="checkbox" name="checkbox">')
                        var $isState = $('<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#updateModal" ng-msg="accept" value="接单">' +
                            '<input class="btn btn-danger" type="button" data-toggle="modal" data-target="#rejectModal"  ng-msg="reject" value="拒绝">')
                        var $detail = $('<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#detailModal" ng-msg="detail" value="查看详情">')
                        var $operation1 = $('<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#updateModal" ng-msg="operation1" value="转生产计划">')
                        var $operation = $('<input class="btn btn-danger" type="button"  ng-msg="del" value="删除">')

                        for (var j = 0; j < feilds.length; j++) {
                            var $td = $("<td name=" + $ths.eq(j).attr("ng-feild") + "></td>")
                            
                            $td.text(o[feilds[j]])

                            if ($ths.eq(j).attr("ng-feild") == "checkbox") {
                                $td.html($check)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "isState") {
                                $td.html($isState)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "detail") {
                                $td.html($detail)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "operation") {
                                $td.html($operation)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "operation1") {
                                $td.html($operation1)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "operation2") {
                                $td.html($operation)
                            }
                            if($ths.eq(j).attr("ng-feild") == "oState"){
                            	if(1 == o[feilds[j]]){
                            		$td.text("待接单")
                            	}
                            	//待接单[1]/已拒绝[2]/已接单[3]/生产中[4]/已完成[5]/删除[-1]
                            	if(2== o[feilds[j]]){
                            		$td.text("已拒绝")
                            	}
                            	if(3== o[feilds[j]]){
                            		$td.text("已接单")
                            	}
                            	if(4== o[feilds[j]]){
                            		$td.text("生产中")
                            	}
                            	if(5== o[feilds[j]]){
                            		$td.text("已完成")
                            	}
                            }
                            if($ths.eq(j).attr("ng-feild") == "oSourse"){
                            	if(1 == o[feilds[j]]){
                            		$td.text("线上")
                            	}else{
                            		$td.text("线下")
                            	}
                            }
                            
                            if($ths.eq(j).attr("ng-feild") == "oProduct.proName"){
                                $td.html(o.oProduct.proName)
                            }
                            if($ths.eq(j).attr("ng-feild") == "oDeadline"){
                                $td.html(new Date(o[feilds[j]]).toLocaleString().replace(/:\d{1,2}$/,' '))

                            }


                            $tr.append($td)
                        }

                        $tbody.append($tr)
                    }
                },
                dataType: "json"
            })
        })
    }

})