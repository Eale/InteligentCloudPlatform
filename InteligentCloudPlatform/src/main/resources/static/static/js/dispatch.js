$(function () {
	 var page = 1
	 var total = 5
	 var pages = 100
	 var list
	 var beginTime=null
	 var endTime=null
	 
	//获取订单
	 getDispatch(beginTime,endTime,page, total)
	 
	    // 没有显示记录数
	    $pageSize = $(".pageSize")
	    $pageSize.change(function () {
	        total = $pageSize.val()
	        if(null!=beginTime&&null!=endTime){
	             getOrder(beginTime,endTime,page, total)
	        }else {
	             getOrder(beginTime,endTime,page, total)
	        }

	    })

	    // 刷新
	    $("#res").click(function () {
	        page=1
	        $formsearch.find("input").val("")
	        beginTime=null
	        endTime=null
	        $("table[ng-src]").attr("ng-src","/dispatch/Undispatch.do");
	        getOrder(beginTime,endTime,page, total)
	    })
	 
	/**
     * 获取列表
     * @param page
     * @param total
     */
    function getDispatch(beginTime,endTime,page, total) {
        $("table[ng-src]").each(function () {
            var $this = $(this);
            var $tbody = $this.children("tbody")
            $tbody.empty()
            $.ajax({
                url: $this.attr("ng-src"),
                type: "get",
                data: {
                    "beginTime": beginTime,
                    "endTime": endTime,
                    "page": page - 1,
                    "total": total
                },
                success: function (obj) {
                    if (null == obj.data) {
                        alert(obj.msg)
                        return
                    }
                    list = obj.data;
                    var $ths = $this.find('tr[ng-tilte="bingTable"]').find("th");
                    var feilds = [];
                    $ths.each(function () {
                        feilds.push($(this).attr("ng-feild"));
                    })


                    for (var i = 0; i < list.length; i++) {
                        var o = list[i]

                        var $tr = $("<tr ng-fid=" + list[i].oId + "></tr>")

                        var $check = $('<input type="checkbox" name="checkbox">')
                        var $isState = $('<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#updateModal" ng-msg="accept" value="接单">' +
                            '<input class="btn btn-danger" type="button" data-toggle="modal" data-target="#rejectModal"  ng-msg="reject" value="拒绝">')
                        var $detail = $('<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#updateModal" ng-msg="detail" value="查看详情">')
                        var $operation1 = $('<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#updateModal" ng-msg="operation1" value="完成">')
                        var $operation = $('<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#updateModal" ng-msg="launch" value="启动">' +
                        		'<input class="btn btn-primary" type="button"  data-toggle="modal" data-target="#updateModal" ng-msg="update" value="修改">' +
                        		'<input class="btn btn-danger" type="button"  ng-msg="del" value="删除">')

                        for (var j = 0; j < feilds.length; j++) {
                            var $td = $("<td name=" + $ths.eq(j).attr("ng-feild") + "></td>")
                            $td.text(o[feilds[j]])
                            if ($ths.eq(j).attr("ng-feild") == "checkbox") {
                                $td.html($check)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "operation") {
                                $td.html($operation)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "operation1") {
                                $td.html($operation1)
                            }
                            if($ths.eq(j).attr("ng-feild") == "disProductionPlan.planId"){
                            	$td.html(o[feilds[j]])
                            }
                            if($ths.eq(j).attr("ng-feild") == "disProductionPlan.planNumber"){
                            	$td.html(o[feilds[j]])
                            }
                            if($ths.eq(j).attr("ng-feild") == "disequipment.equipSequence"){
                            	$td.html(o[feilds[j]])
                            }
                            if($ths.eq(j).attr("ng-feild") == "disProductionPlan.planOrder.oProduct.proName"){
                            	$td.html(o[feilds[j]])
                            }
                            
                            if($ths.eq(j).attr("ng-feild") == "dispState"){
                            	if(1 == o[feilds[j]]){
                            		$td.text("未启动")
                            	}
                            	if(2 == o[feilds[j]]){
                            		$td.text("已启动")
                            	}
                            	if(3 == o[feilds[j]]){
                            		$td.text("已完成")
                            	}
                            	if(-1 == o[feilds[j]]){
                            		$td.text("已删除")
                            	}
                            }
                            if($ths.eq(j).attr("ng-feild") == "oSourse"){
                            	if(1 == o[feilds[j]]){
                            		$td.text("线上")
                            	}else{
                            		$td.text("线下")
                            	}
                            }
                            
                            $tr.append($td)
                        }

                        $tbody.append($tr)
                    }
                },
                dataType: "json"
            })
        })
    }
	 
	 
	    
})