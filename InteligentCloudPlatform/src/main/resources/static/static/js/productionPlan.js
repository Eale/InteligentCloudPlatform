$(function () {
    var page = 1
    var total = 5
    var pages = 100
    var list
    var beginTime = null
    var endTime = null


    var $selectOrder = $("#selectOrder")
    var $addPlanForm = $("#addPlanForm")
    $selectOrder.change(function () {
        var oId = $(this).val()
        var data = list.find(function (e) {
            return e.oId == oId
        })
        var date = new Date(data.oDeadline)
        $addPlanForm.find('input[name="planOrder.oProduct.proName"]').val(data.oProduct.proName)
        $addPlanForm.find('input[name="planOrder.oQuantity"]').val(data.oQuantity)
        $addPlanForm.find('input[name="planOrder.oDeadline"]').val(date.getUTCFullYear() + "-" + (date.getUTCMonth() + 1) + "-" + date.getUTCDate())
    })

    //获取未启动计划
    $("body").delegate('a[data-src="../Static/productionPlan/Production-plan.htm"]', 'click', function () {
        $.ajax({
            type: "get",
            url: "/productionPlan/produtcByStatePage.do",
            data: {
                "beginTime": beginTime,
                "endTime": endTime,
                "page": page - 1,
                "total": total
            },
            dataType: 'json',
            success: function (obj) {
                //
            }
        });
    })

    //获取以启动计划
    $("body").delegate('a[data-src="../Static/productionPlan/Production-planlaunched.htm"]', 'click', function () {
        $.ajax({
            type: "get",
            url: "/productionPlan/produtcByStatePagelaunched.do",
            data: {
                "beginTime": beginTime,
                "endTime": endTime,
                "page": page - 1,
                "total": total
            },
            dataType: 'json',
            success: function (obj) {
                //
            }
        });
    })


    //添加计划
    $("#addPlanBtn").click(function () {
        $.ajax({
            type: "post",
            url: "/productionPlan/productionPlan.do",
            data: {
                "planOrder.oNumber": $("select[name='planOrder.oNumber']").val(),
                "planNumber": $("input[name='planOrder.oQuantity']").val(),
                "planOrder.oDeadline": Date.parse($("input[name='planOrder.oDeadline']").val()),
                "planStarttime": Date.parse($("input[name='planStarttime']").val()),
                "planEndtime": Date.parse($("input[name='planEndtime']").val()),
            },

            dataType: 'json',
            success: function (obj) {
                if (null != obj.msg) {
                    alert(obj.msg)
                    return
                }
            }
        });
    })

    //获取订单列表
    $("body").delegate('a[data-src="../Static/productionPlan/NewPlan-ADMINISTER.htm"]', 'click', function () {

        $.ajax({
            type: "get",
            url: "/order/searchOrderState.do",
            dataType: 'json',
            data: {
                "oState": 3,
                "page": 0,
                "total": 9999,
            },
            success: function (obj) {
                var data = obj.data
                list = data
                var $selectOrder = $("#selectOrder")
                $selectOrder.empty()
                for (var i = 0; i < data.length; i++) {
                    $selectOrder.append("<option value='" + data[i].oId + "'>" + data[i].oNumber + "</option>")
                }
            }
        });
    })
})