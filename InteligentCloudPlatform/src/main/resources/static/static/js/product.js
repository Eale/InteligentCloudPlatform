$(function () {
    var page = 1
    var total = 5
    var pages = 100
    var list
    var beginTime=null
    var endTime=null

    //获取产品
    getProduct(beginTime,endTime,page, total)

    //没有显示记录数
    $pageSize = $(".pageSize")
    $pageSize.change(function () {
        total = $pageSize.val()
        if(null!=beginTime&&null!=endTime){
            getProduct(beginTime,endTime,page, total)
        }else {
            getProduct(beginTime,endTime,page, total)
        }

    })

    //加载page页面
    $(".page").load($(".page").attr("data-src"))

    //获取所有产品名称


    //刷新
    $("#res").click(function () {
        page=1
        $formsearch.find("input").val("")
        beginTime=null
        endTime=null
        $("table[ng-src]").attr("ng-src","/product/searchProductCondition.do");
        getProduct(beginTime,endTime,page, total)
    })

    //搜索
    var $search = $("#search")
    var $formsearch=$(".form-search")

    $search.click(function () {
        page = 1
        $("table[ng-src]").each(function () {
            beginTime=Date.parse($formsearch.find('input[name="begintime"]').val())
            endTime=Date.parse($formsearch.find('input[name="endtime"]').val())
            $("table[ng-src]").attr("ng-src","/product/searchProductTime.do");
            getProduct(beginTime,endTime,page, total)

        })
    })


    /**
     * 显示一条记录
     */
    $('body').delegate('input[ng-msg="update"]', 'click', function () {
        var $this = $(this);
        var proFid = $this.parents("tr").attr("ng-fid")

        var data = list.find(function (e) {
            return e.proFid == proFid
        })
        $model = $("#updateModal")
        console.log($model.find('input[name="proFid"]').attr('name'))
        $model.find('input[name="proFid"]').val(data.proFid)
        $model.find('input[name="proName"]').val(data.proName)
        $model.find('input[name="proImgurl"]').val(data.proImgurl)
        $model.find('input[name="proDescribe"]').val(data.proDescribe)
        $model.find('input[name="proState"]').val(data.proState)

        $model.find('button[id="updateProduct"]').attr('ng-fid', data.proFid)

    })
    /**
     * 修改
     */
    $('body').delegate('#updateProduct', 'click', function () {
        $.ajax({
            type: "put",
            url: "/product/product.do",
            data: $(".updateProductForm").serialize(),
            dataType: 'json',
            success: function (obj) {
                getProduct(beginTime,endTime,page, total)
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })
    /**
     * 删除
     */
    $('body').delegate('input[ng-msg="del"]', 'click', function () {
        var $this = $(this);
        var proFid = $(this).parents("tr").attr("ng-fid")
        $.ajax({

            type: "delete",
            url: "/product/product.do",
            data: {
                "proFid": proFid
            },
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    $this.parents("tr").remove()
                }


            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 全选
     */
    var $checkAll = $("#checkAll")
    $checkAll.change(function () {
        var isChecked = $checkAll.prop("checked")
        $checkAll.parents("table").find("input").prop("checked", isChecked)
    })

    /**
     * 批量删除
     */

    var $dels = $("#dels")
    $dels.click(function () {
        var proFids=[]
        $(".table input[name='checkbox']:checked").each(function () { // 遍历选中的checkbox
            proFid = $(this).parents("tr").attr("ng-fid"); // 获取checkbox所在行的顺序
            proFids.push(proFid)
        })
        console.log(proFids)
        $.ajax({

            type: "delete",
            url: "/product/productS.do",
            data: {
                "proFids": proFids
            },
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    for (var i = 0; i < proFids.length; i++) {
                        $(".table").find("tr[ng-fid="+proFids[i]+"]").remove()
                    }
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 点击添加按钮
     */
    $("#addBtn").click(function () {
        $.ajax({
            type: "get",
            url: "/factory/factorysByState.do",
            dataType: 'json',
            success: function (obj) {
                var data = obj.data
                var $selectFactory = $("#selectFactory")
                $selectFactory.empty()
                for (var i = 0; i < data.length; i++) {
                    $selectFactory.append("<option>" + data[i].fName + "</option>")
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })
    /**
     * 增加
     */
    $('body').delegate('#addProduct', 'click', function () {
        $.ajax({
            type: "post",
            url: "/product/product.do",
            data: $(".addProductForm").serialize(),
            dataType: 'json',
            success: function (obj, state, xhr) {
                if (null != obj.msg) {
                    alert(obj.msg)
                    return
                }
                if (xhr.readyState == 4 && xhr.status == 200) {
                    getProduct(beginTime,endTime,page, total)
                }
            },
            error: function (error, textStatus) {
                $("button").attr('disabled', false);
                if (textStatus == 'timeout') {
                    alert('请求超时!请检查网络');
                } else {
                    alert('请求出错');
                }
            }
        });
    })

    /**
     * 分页
     */

    // $page = $(".page")
    $('body').delegate('.page li', 'click', function () {
        // $page.find("li").click(function () {
        $this = $(this)
        $previous = $this.find('a[aria-label="Previous"]')
        $next = $this.find('a[aria-label="Next"]')

        //上一页
        function previous() {
            if (page > 1) {
                page--;
               getProduct(beginTime,endTime,page, total)
            } else {
                alert("没有上一页啦!")
            }
            return
        }

        //下一页
        function next() {
            if (page < pages) {
                page++;
               getProduct(beginTime,endTime,page, total)
            } else {
                alert("没有下一页啦!")
            }
            return
        }

        btnpage = $this.find("a").text()
        if (btnpage == "pre") {
            previous()
            $(".thisPage").val(page)
            return
        }
        if (btnpage == "next") {
            next()
            $(".thisPage").val(page)
            return
        }

        $(".thisPage").val(page)
        getProduct(beginTime,endTime,page, total)



    })


    /**
     * 获取列表
     * @param page
     * @param total
     */
    function getProduct(beginTime,endTime,page, total) {
        $("table[ng-src]").each(function () {
            var $this = $(this);
            var $tbody = $this.children("tbody")
            $tbody.empty()
            $.ajax({
                url: $this.attr("ng-src"),
                type: "get",
                data: {
                    "beginTime":beginTime,
                    "endTime":endTime,
                    "page": page - 1,
                    "total": total
                },
                success: function (obj) {
                    if(null==obj.data){
                        alert(obj.msg)
                        return
                    }
                    list = obj.data;
                    var $ths = $this.find('tr[ng-tilte="bingTable"]').find("th");
                    var feilds = [];
                    $ths.each(function () {
                        feilds.push($(this).attr("ng-feild"));
                    })

                    for (var i = 0; i < list.length; i++) {
                        var o = list[i]

                        var $tr = $("<tr ng-fid=" + list[i].proFid + "></tr>")

                        var $check = $('<input type="checkbox" name="checkbox" >')
                        var $operation = $('<input class="btn btn-primary" type="button"   data-toggle="modal" data-target="#updateModal" ng-msg="update" value="修改">' +
                            '<input class="btn btn-danger" type="button"   ng-msg="del" value="删除">')
                        for (var j = 0; j < feilds.length; j++) {
                            var $td = $("<td name=" + $ths.eq(j).attr("ng-feild") + "></td>")
                            if ($ths.eq(j).attr("ng-feild") == "checkbox") {
                                $td.html($check)
                            }
                            if ($ths.eq(j).attr("ng-feild") == "operation") {
                                $td.html($operation)
                            }
                            $td.text(o[feilds[j]])
                            $tr.append($td)
                        }

                        $tbody.append($tr)
                    }
                },
                dataType: "json"
            })
        })
    }
})