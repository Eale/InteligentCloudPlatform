$(function () {
    $('div[data-src]').html(function () {
        var html
        var $this = $(this)
        var src = $this.attr('data-src')
        $.ajax({
            type: "get",
            url: src,
            async: false,
            success: function (text) {
                html = text
            }
        })
        return html
    })

})

