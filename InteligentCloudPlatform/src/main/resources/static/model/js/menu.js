$(function () {
    $('body').delegate('.panel-group a', 'click', function () {
        var $this = $(this)
        var $breadcrumb=$('.breadcrumb')

        if($this.attr("data-parent")=="#accordion"){
            $breadcrumb.empty()
            $breadcrumb.append('<li class="active">'+$this.text()+'</li>')
        }else {
            $breadcrumb.empty()
            $breadcrumb.append('<li>'+$this.attr("data-parent")+'</li>')
            $breadcrumb.append('<li class="active">'+$this.text()+'</li>')
        }

    })
})