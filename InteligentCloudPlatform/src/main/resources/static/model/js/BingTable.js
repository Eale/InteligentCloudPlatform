/**
 * 用于列表绑定表格
 * 作者:Chiva 874460517@qq.com
 * 日期:2018年11月19日
 */
$(function () {

    $("table[ng-src]").each(function () {
        var $this = $(this);

        $.ajax({
            url: $this.attr("ng-src"),
            type: "get",
            success: function (obj) {
                var list = obj.data;
                var $ths = $this.find('tr[ng-tilte="bingTable"]').find("th");
                var feilds = [];
                $ths.each(function () {
                    feilds.push($(this).attr("ng-feild"));
                })

                var $table = $this;

                for (var i = 0; i < list.length; i++) {
                    var o = list[i];

                    var $tr = $("<tr></tr>");
                    for (var j = 0; j < feilds.length; j++) {
                        var $td = $("<td></td>");
                        $td.text(o[feilds[j]]);
                        $tr.append($td)
                    }

                    $table.append($tr);
                }
            },
            dataType: "json"
        })
    })
})
