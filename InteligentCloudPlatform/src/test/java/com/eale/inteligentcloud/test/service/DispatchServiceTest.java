
package com.eale.inteligentcloud.test.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.eale.inteligentcloud.domain.ICPDispatch;
import com.eale.inteligentcloud.domain.ICPFollow;
import com.eale.inteligentcloud.domain.ICPProductionPlan;
import com.eale.inteligentcloud.repository.FollowRepository;
import com.eale.inteligentcloud.repository.ProductionPlanRepository;
import com.eale.inteligentcloud.service.DispatchService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DispatchServiceTest {
	
	@Autowired
	DispatchService dispatchservice;
	@Autowired
	ProductionPlanRepository productionPlanRepository;
	@Autowired
	FollowRepository followRepository;
	
	@Test
	public void testAdddispatch() {
		ICPDispatch dispatch=new ICPDispatch();
		dispatch.setDispBegintiem((long) 001);
		dispatch.setDispCtime(new Date().getTime());
		dispatch.setDispCuser((long) 911);
		dispatch.setDispEndtime(new Date().getTime());
		dispatch.setDispState(1);
		dispatch.setDispUptime(new Date().getTime());
		dispatch.setDispUpuser((long) 911);
		
		 ICPProductionPlan producttionPlan = new ICPProductionPlan();
		 producttionPlan.setPlanId((long)44);
		 Example<ICPProductionPlan> example=Example.of(producttionPlan);
		 List<ICPProductionPlan> list = productionPlanRepository.findAll(example);
		
		 List<ICPFollow> list2  = new ArrayList<>();
		 ICPFollow follow=new ICPFollow();
		 follow.setFollowCtime(new Date().getTime());
		 follow.setFollowCuser((long) 111);
		 follow.setFollowEndTime(new Date().getTime());
		 follow.setFollowProcessNum(4);
		 follow.setFollowQualifyNum(1);
		 follow.setFollowState(1);
		 follow.setFollowStratTime(new Date().getTime());
		 follow.setFollowUptime(new Date().getTime());
		 follow.setFollowUpuser((long) 1221);
		 followRepository.save(follow);
		 
		 list2.add(follow);
		 
		 follow=new ICPFollow();
		 follow.setFollowCtime(new Date().getTime());
		 follow.setFollowCuser((long) 111);
		 follow.setFollowEndTime(new Date().getTime());
		 follow.setFollowProcessNum(4);
		 follow.setFollowQualifyNum(1);
		 follow.setFollowState(1);
		 follow.setFollowStratTime(new Date().getTime());
		 follow.setFollowUptime(new Date().getTime());
		 follow.setFollowUpuser((long) 1221);
		 followRepository.save(follow);
		 
		 list2.add(follow);
		 
		 dispatch.setFollows(list2);
		 dispatch.setDisProductionPlan(list.get(0));
		 dispatchservice.adddispatch(dispatch);
		
	}

	@Test
	public void testDeletedispatch() {
		Long dispFid = (long) 7;
		dispatchservice.deletedispatch(dispFid);
	}

	@Test
	public void testDeletedispatches() {
		Long[] arr= { (long) 7,(long) 14};
		
		dispatchservice.deletedispatches(arr);
	}

	@Test
	public void testLaunch() {
//		Long dispFid = (long) 7;
//		dispatchservice.launch(dispFid);
	}

	//未测试关联生产计划和报工单的修改
	@Test
	public void testUpdatedispatch() {
		ICPDispatch dispatch = new ICPDispatch();
		dispatch.setDispFid((long) 7);
		dispatch.setDispBegintiem((long) 888);
		dispatch.setDispCtime(new Date().getTime());
		dispatch.setDispCuser((long) 888);
		dispatch.setDispEndtime(new Date().getTime());
		dispatch.setDispState(5);
		dispatch.setDispUptime(new Date().getTime());
		dispatch.setDispUpuser((long) 888);
		
		dispatchservice.updatedispatch(dispatch);
	}

	@Test
	public void testFindById() {
		Long dispFid = (long) 7;
		
		Optional<ICPDispatch> opt = dispatchservice.findById(dispFid);
		System.out.println(opt.get().getFollows());
	}

	@Test
	public void testFindByPage() {
		Pageable page=PageRequest.of(0, 3);
		List<ICPDispatch> list = dispatchservice.findByPage(page);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getDispFid());
		}
	}

	@Test
	public void testFindByCondetions() {
		Pageable page=PageRequest.of(0, 3);
		ICPDispatch dispatch = new ICPDispatch();
		dispatch.setDispState(1);
//		dispatch.setDispCuser((long) 789);
		
		List<ICPDispatch> list = dispatchservice.findByCondetions(page, dispatch);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getDispFid());
		}
		
	}

	@Test
	public void testFindByTime() {
		Pageable page=PageRequest.of(0, 3);
		Long startTime = (long) 1543042445*1000 +295;
		Long endTime = (long) 1543043703*1000+754;
		List<ICPDispatch> list = dispatchservice.findByTime(page, startTime, endTime);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getDispFid());
		}
	}

	@Test
	public void testFindcount() {
		int mun=dispatchservice.findcount();
		System.out.println(mun);
	}

	@Test
	public void testFindByStatePage() {
		Pageable page=PageRequest.of(0, 3);
		int followState = 0;
		
		List<ICPDispatch> list = dispatchservice.findByStatePage(page, followState);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getDispFid());
		}
		
	}

}
