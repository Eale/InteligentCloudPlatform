package com.eale.inteligentcloud.test.service;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.eale.inteligentcloud.domain.ICPEquipment;
import com.eale.inteligentcloud.domain.ICPFactory;
import com.eale.inteligentcloud.domain.ICPProduct;
import com.eale.inteligentcloud.service.EquipmentService;
import com.eale.inteligentcloud.service.FactoryService;
import com.eale.inteligentcloud.service.ProductService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EquipmentServiceTest {
	
	@Autowired
	EquipmentService equipmentservice;
	
	@Autowired
	FactoryService factoryservice;
	
	@Autowired
	ProductService productservice;
	
	@Test
	public void testAddEquipment() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipName("组装机");
		equipment.setEquipCapacity(200.0);
		equipment.setEquipSequence("EQ_11111101");
		equipment.setEquipState(1);
		equipment.setEquipImgur("d//");
		equipment.setEquipTime(new Date().getTime());
		equipment.setEquipCtime(new Date().getTime());
		equipment.setEquipCuser((long)2);
		
		List<ICPFactory> list = factoryservice.findByFactoryName("XXXX公司");
		
		
		equipment.setEquipfactory(list.get(0));
		
		ICPProduct product=new ICPProduct();
		product.setProName("333");
		Pageable page=PageRequest.of(0, 1);
		List<ICPProduct> list2 = productservice.findByConditionsPage(page, product);
		
		equipment.setEquipProduct(list2.get(0));
		
		equipmentservice.addEquipment(equipment);
		
		
	}

	@Test
	public void testFindEquipType() {
		ICPEquipment equipment=new ICPEquipment();
		equipment.setEquipState(2);
		int type = equipmentservice.findEquipType(equipment);
		System.out.println(type);
		
	}

	@Test
	public void testFigureEquipTypeRate() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteEquipment() {
		Long[] ids= {(long)28,(long)29};
		equipmentservice.DeleteEquipment(ids);
	}

	@Test
	public void testUpdateEquipment() {
		
	}

	@Test
	public void findByCondtions() {
		ICPEquipment equipment=new ICPEquipment();
		ICPProduct product=new ICPProduct();
		product.setProFid((long)5);
		equipment.setEquipProduct(product);
		equipment.setEquipState(1);
		System.out.println(product.toString());
		System.out.println(equipment.toString());
		List<ICPEquipment> equipmentlist = equipmentservice.findByCondtions(equipment);
		for (ICPEquipment icpEquipment : equipmentlist) {
			System.out.println(icpEquipment.toString());
		}
	}

	@Test
	public void testFindByTimePage() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPage() {
		Pageable page=PageRequest.of(0, 2);
		List<ICPEquipment> list = equipmentservice.findAllPage(page);
		for (ICPEquipment icpEquipment : list) {
			System.out.println(icpEquipment.toString());
		}
	}

	@Test
	public void testFindNUmber() {
		int num=equipmentservice.findNUmber();
		System.out.println(num);
	}

	@Test
	public void testFindById() {
		Optional<ICPEquipment> optional = equipmentservice.findById((long)28);
		System.out.println(optional.toString());
	}

	@Test
	public void testDeleteById() {
		equipmentservice.deleteById((long)28);
	}

}
