package com.eale.inteligentcloud.test.controller;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.eale.inteligentcloud.common.Result;
import com.eale.inteligentcloud.controller.OrderController;
import com.eale.inteligentcloud.domain.ICPOrder;
import com.eale.inteligentcloud.domain.ICPProduct;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrdercontrollerTest {
	
	@Autowired
	OrderController ordercontroller;
	
	@Test
	public void testOrderController() {
		ICPOrder order=new ICPOrder();
		order.setoDeadline(new Date().getTime());
		order.setoNumber(new Date().getTime());
		order.setoSourse(1);
		order.setoPlacetime(new Date().getTime());
		
		ICPProduct product=new ICPProduct();
		product.setProName("测试3");
		
		order.setoProduct(product);
		order.setoQuantity((long)10000);
//		ordercontroller.addOrder(order);
	}
	
	@Test
	public void testgetByStatehandled() {
		Result rs = (Result) ordercontroller.getByStatehandled(0, 5);
		System.out.println(rs.getData().toString());
		
	}
}
