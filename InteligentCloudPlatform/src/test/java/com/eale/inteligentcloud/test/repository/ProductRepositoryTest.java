package com.eale.inteligentcloud.test.repository;

import java.util.List;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.eale.inteligentcloud.domain.ICPProduct;
import com.eale.inteligentcloud.repository.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTest {

	@Autowired
	ProductRepository productRepository;

	// 根据ID查找
	@Test
	public void findByID() {
		Long proFid = (long) 1;
		Optional<ICPProduct> product = productRepository.findById(proFid);
		System.out.println(product.get().toString());
	}

	// 根据时间 分页查询产品
	@Test
	public void selectByPage() {
		long startTime = (long) (1542872012 * 1000 + 860);
		long endTime = (long) (1542873190 * 1000 + 479);
		Pageable page = PageRequest.of(1, 3);

		List<ICPProduct> list = productRepository.findByPageAndTime(startTime, endTime,page);

		System.out.println(list.size());
		for (ICPProduct icpProduct : list) {
			System.out.println(icpProduct.toString());
		}

	}
}
