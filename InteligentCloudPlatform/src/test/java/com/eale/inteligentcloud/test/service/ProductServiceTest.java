
package com.eale.inteligentcloud.test.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.eale.inteligentcloud.domain.ICPProduct;
import com.eale.inteligentcloud.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
	@Autowired
	ProductService productService;

	
	
	// 产品增加
	@Test
	public void addProduct() {
		ICPProduct product = new ICPProduct();
		product.setProName("测试3");
		product.setProDescribe("service测试3");
		product.setProCtime(new Date().getTime());
		product.setProState(3);
		product.setProCuser((long) 003);
		product.setProUptime(new Date().getTime());
		product.setProUpuser((long) 003);
		product.setProImgurl("ABCDEFGh");

		productService.addProduct(product);
	}
	
	
	// 产品删除
	@Test
	public void deleteProduct() {
		productService.deleteProduct((long) 18);
	}
	
	// 产品批量删除
	@Test
	public void deleteProducts() {

		Long[] proFis = { (long) 17, (long) 18, (long) 19 };

		productService.deleteProducts(proFis);
	}
	
	// 修改产品信息
	@Test
	public void updatePro() {
		ICPProduct product = new ICPProduct();
		product.setProFid((long) 5);
		product.setProName("测试4");
		product.setProDescribe("service测试4");
		product.setProCtime(new Date().getTime());
		product.setProState(2);
		product.setProCuser((long) 002);
		product.setProUptime(new Date().getTime());
		product.setProUpuser((long) 002);
		product.setProImgurl("ABCDEFG");

		productService.updateProduct(product);

	}
	
	
	//按条件分页查询
	@Test
	public void findByConditonsPage() {
		Pageable page=PageRequest.of(0, 4);
		ICPProduct product=new ICPProduct();
		product.setProName("测试3");
		List<ICPProduct> list = productService.findByConditionsPage(page, product);
		System.out.println(list.size());
		for (ICPProduct icpProduct : list) {
			System.out.println(icpProduct.toString());
		}
	}
	
	
	//分页查询全部
	@Test
	public void findAll() {
		Pageable page=PageRequest.of(1, 3);
		
		List<ICPProduct> list = productService.findAllPage(page);
		
		for (ICPProduct icpProduct : list) {
			System.out.println(icpProduct.toString());
			
		}
		

	}
	

	//根据ID查询
	@Test
	public void findbyId() {
		Long proFid=(long) 5;
		Optional<ICPProduct> product=productService.findById(proFid);
		System.out.println(product.get().toString());
	}
	

	// 根据时间 分页查询产品
	@Test
	public void selectByPage() {
		long startTime=(long) 1444;
		long endTime=(long) 1543022317 * 1000 + 479;
		Pageable page=PageRequest.of(0, 3);

		List<ICPProduct> list = productService.findConditionsPage(startTime,endTime,page);
		System.out.println(list.size());
		for (ICPProduct icpProduct : list) {
			System.out.println(icpProduct.toString());
		}

	}

	//根据记录数量
	@Test
	public void findAllNum() {
		long number=productService.findAllNum();
		System.out.println(number);
	}
	
	
}
