package com.eale.inteligentcloud.test.service;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.eale.inteligentcloud.domain.ICPAdminstrator;
import com.eale.inteligentcloud.domain.ICPFactory;
import com.eale.inteligentcloud.service.AdminstratorService;
import com.eale.inteligentcloud.service.FactoryService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminstratorServiceTest {
	
	@Autowired
	AdminstratorService adminstratorservice;
	
	@Autowired
	FactoryService factoryservice;
	
	
	@Test
	public void testAddAdmin() {
		ICPAdminstrator admins=new ICPAdminstrator();
		admins.setAdId((long)1);
		admins.setAdNumber("1111105");
		admins.setAdPassword("555555");
		admins.setAdGrade(2);
		admins.setAdGrade(2);
		admins.setAdDelete(1);
		admins.setAdCtime(new Date().getTime());
		admins.setAdCuser((long)1);
		
		long getfid=(long) 1;
		Optional<ICPFactory> opt = factoryservice.findById(getfid);
		if(opt.isPresent()) {
			admins.setAdfactory(opt.get());
			adminstratorservice.addAdmin(admins);
		}
		//return new Result(1,null,"请输入正确的工厂");
		
		
		
	}

	@Test
	public void testDeleteAdmin() {
		Long adid=(long) 2;
		adminstratorservice.deleteAdmin(adid);
	}

	@Test
	public void testUpdateAdmin() {
		ICPAdminstrator admins=new ICPAdminstrator();
		admins.setAdId((long)4);
		admins.setAdGrade(1);
		
		Object opt = adminstratorservice.updateAdmin(admins);
		System.out.println(opt.toString());
	}

	@Test
	public void testFindAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testLogin() {
		
		String number=1111105+"";
//		Long adId=(long) 1;
		String passwpor="555555";
		ICPAdminstrator admin=new ICPAdminstrator();
		admin.setAdNumber(number);
		admin.setAdPassword(passwpor);
		
		Optional<ICPAdminstrator> optional1 = adminstratorservice.login(admin);
		System.out.println(optional1.get().toString());

	}

	@Test
	public void testDeleteAdmins() {
		Long[] adids= {(long)2,(long)4};
		adminstratorservice.deleteAdmins(adids);
	}

	@Test
	public void testFindByPage() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindByPageTime() {
		Long startTime=(long) 1543040792;
		Long endTime=(long) 1543040793*1000;
		Pageable pageable=PageRequest.of(0, 2);
		List<ICPAdminstrator> list = adminstratorservice.findByPageTime(pageable, startTime, endTime);
		for (ICPAdminstrator icpAdminstrator : list) {
			System.out.println(icpAdminstrator.toString());
		}
		
	}
	
	@Test
	public void testFindByPageTimeState() {
		Long startTime=(long) 1543040792;
		Long endTime=(long) 1543040793*1000;
		int state=1;
		Pageable pageable=PageRequest.of(0, 2);
		List<ICPAdminstrator> list = adminstratorservice.findByPageTimeState(pageable, startTime, endTime,state);
		for (ICPAdminstrator icpAdminstrator : list) {
			System.out.println(icpAdminstrator.toString());
		}
		
	}

}
