package com.eale.inteligentcloud;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.eale.inteligentcloud.domain.ICPProduct;
import com.eale.inteligentcloud.repository.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InteligentCloudPlatformApplicationTests {
	
	@Autowired
	ProductRepository productRepository;
	
	@Test
	public void addProduct() {
		ICPProduct product = new ICPProduct();
		product.setProName("测试产品1");
		product.setProImgurl("产品图片1");
		product.setProDescribe("产品描述1");
		product.setProState(1);
		
		product.setProCtime(new Date().getTime());
		//product.setProCuser("测试人员1");
		product.setProUptime(new Date().getTime());
		//product.setProUpuser("修改人员1");
		
		productRepository.save(product);
		
	}

}
